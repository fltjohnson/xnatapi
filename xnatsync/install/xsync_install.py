__author__ = 'fjohnson'
import subprocess
import glob
import os
import sys
import site
import shutil
from ast import literal_eval
from tempfile import NamedTemporaryFile

'''xnatsync install script.'''

conf_path = os.path.join(os.getcwd(),'config')
if not os.path.exists(conf_path):
    print >> sys.stderr, 'Config file %s must exist'%conf_path
os.chdir('/tmp')

#download necessary packages
for pat in ['shell-utils-HEAD-*','xnatapi-HEAD-*']:
    for f in glob.glob(pat):
        if os.path.isdir(f):
            shutil.rmtree(f)
        else:os.unlink(f)

def c(command):
    subprocess.check_call(command)

c(['wget','-O','xnatapi-HEAD-stuff.tar.gz','http://ninja.rotman-baycrest.on.ca/git/?p=fjohnson/xnatapi.git;a=snapshot;h=HEAD;sf=tgz'])
c(['wget','-O','shell-utils-HEAD-stuff.tar.gz','http://ninja.rotman-baycrest.on.ca/git/?p=fjohnson/shell-utils;a=snapshot;h=HEAD;sf=tgz'])
c(['tar','xzf',glob.glob('shell-utils-HEAD-*.tar.gz').pop()])
c(['tar','xzf',glob.glob('xnatapi-HEAD-*.tar.gz').pop()])

for pat in ['shell-utils-HEAD-*.tar.gz','xnatapi-HEAD-*.tar.gz']:
    for f in glob.glob(pat): os.unlink(f)

os.chdir('/etc/init.d/')
for initscript in glob.glob('xnatsync_*.py'):
    c(('service %s stop' % initscript).split())
os.chdir('/tmp')

os.chdir(glob.glob('xnatapi-HEAD-*').pop())
# os.chdir('/root/xnatapi')
c(['python','setup.py','install'])

os.chdir('..')
os.chdir(glob.glob('shell-utils-HEAD-*').pop())
# os.chdir('/root/shell-utils')
c(['python','setup.py','install'])

if not os.path.exists('/root/.xnat_sync.conf'):
    with open('/root/.xnat_sync.conf','w') as xsconf:
        xsconf.write('{}')

#update config
with open('/root/.xnat_sync.conf','r+') as xsconf:
    config_existing = literal_eval(xsconf.read())

    with open(conf_path) as xsnewconf:
        config_new = literal_eval(xsnewconf.read())

    config_existing.update(config_new)
    config = config_existing

    #reload sys.path so we can use a few functions from the xnatapi package
    reload(site)
    from xnatapi.xnatsync.xnatsync import parse_config, err

    #error check config
    tfile = NamedTemporaryFile()
    tfile.write(str(config))
    tfile.flush()
    os.fsync(tfile)
    parse_config({'conf':tfile.name})
    tfile.close()

    xsconf.seek(0)
    xsconf.truncate()
    xsconf.write(str(config))


#Create init.d scripts for defined packages
initscript = open('/etc/init.d/xnatsync_init.py','r')

#reference of fields that need to be changed
# lockf='/var/run/xnatsync_%s.lock'%__project__
# usage='start|stop|reset'
# preargs=[]
# exe='xnatsync.py'
# args=[__project__]

icontents = initscript.read()
icontents= icontents.replace('# description: Starts and stops X','# description: Starts and stops the XNAT sync',1)
icontents= icontents.replace('# chkconfig: 35 70 80','# chkconfig: 35 90 30',1)
icontents= icontents.replace("'''Template for a chkconfig startup file'''","'''XNAT sync chkconfig startup file'''",1)

for p in config:
    if p != '_Subject Registry' and p != '_hostname':
        pscript=icontents.replace('__project__',"'%s'" % p,2)
        with open('/etc/init.d/xnatsync_%s.py' % p,'w') as pscriptf:
            pscriptf.write(pscript)
        os.chmod('/etc/init.d/xnatsync_%s.py' % p,0500)
        c(['chkconfig', '--add', 'xnatsync_%s.py'% p])
initscript.close()

os.unlink('/etc/init.d/xnatsync_init.py')

#set hostname
with open('/etc/sysconfig/network','r+') as nconfig:
    nc=nconfig.readlines()
    found=False
    for i,l in enumerate(nc):
        if 'HOSTNAME=' in l:
            found=True
            break
    if found:
        nc[i]='HOSTNAME=%s\n'%config['_hostname']
    else:
        nc.append('HOSTNAME=%s\n'%config['_hostname'])

    nconfig.seek(0)
    for l in nc: print >> nconfig, l,
    nconfig.truncate()

os.chdir('/etc/init.d/')
for initscript in glob.glob('xnatsync_*.py'):
    c(('service %s start' % initscript).split())
