__author__ = 'fjohnson'
import subprocess
import site
import os
import glob
import shutil
import sys

'''xnatsync uninstall script.'''

def c(command):
    subprocess.check_call(command)
def p(str):print str

os.chdir('/etc/init.d/')
inits=glob.glob('xnatsync_*.py')
for f in inits:
    if f != 'xnatsync_init.py':
        c(('chkconfig --del %s'%f).split())
        p(('chkconfig --del %s'%f).split())
        c(('service %s stop'%f).split())
        p(('service %s stop'%f).split())
    os.unlink(f)
    p('unlink ' + f)

for f in ['/root/.xnat_sync.conf','/usr/bin/xnatsync.py']+glob.glob('/var/run/xnatsync_*.lock'):
    f=os.path.expanduser(f)
    if os.path.exists(f):
        os.unlink(f)
        p('unlink ' + f)

sitedir = os.path.join(sys.prefix,'lib/python%s/site-packages'%sys.version[0:3])
os.chdir(sitedir)
p('chdir ' + sitedir)
for pat in ['xnatapi-*.egg-info','sysutil-*.egg-info']:
    for f in glob.glob(pat):
        os.unlink(f)
        p('unlink ' + f)

if os.path.exists('xnatapi'):
    shutil.rmtree('xnatapi')
    p('rmtree xnatapi')
if os.path.exists('sysutil'):
    shutil.rmtree('sysutil')
    p('rmtree sysutil')
