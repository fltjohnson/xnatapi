import pyxnat
from xnatapi import getJSON, surljoin, getauthsession

'''run xnatsynctpopulate.py first to create a mock project to sync.
this mock project has no metadata'''

SERVERADDY='https://localhost:8889/spred'
user='admin'
passw='admin'
SESSION=getauthsession(SERVERADDY,user,passw)

def assertFilesSync(o):
    fset=getJSON(surljoin(SERVERADDY,o._uri,'files'),'Name','collection',session=SESSION)
    assert fset == set([('a', ''), ('a', 'rSame'), ('f', 'rNotLocal'), ('b', ''), 
                        ('c', 'rSame'), ('b', 'rSame'), ('c', ''), ('e', 'rNotRemote'), 
                        ('d', 'rSame'), ('d', '')]), fset

def assertFilesNewSync(o):
    fset=getJSON(surljoin(SERVERADDY,o._uri,'files'),'Name','collection',session=SESSION)
    assert fset == set([('a', ''), ('a', 'rSame'), ('e', 'rNotRemote'), ('b', ''), 
                        ('b', 'rSame'), ('c', 'rSame'), ('c', '')]), fset
def assertFilesNoSync(o):
    fset=getJSON(surljoin(SERVERADDY,o._uri,'files'),'Name','collection',session=SESSION)
    assert fset == set([('a', ''), ('a', 'rSame'), ('f', 'rNotLocal'), ('b', ''), 
                        ('b', 'rSame'), ('d', 'rSame'), ('d', '')]), fset

x = pyxnat.Interface(server=SERVERADDY, user=user, password=passw)
p=x.select.project('synctest')
assert p.exists()
assertFilesSync(p)

slabels=map(lambda ID: p.subject(ID).label(), p.subjects().get())
assert set(slabels) == set(['sExists','sNotLocal']), slabels
assertFilesSync(p.subject('sExists'))
assertFilesNoSync(p.subject('sNotLocal'))

sE=p.subject('sExists')
elabels=map(lambda ID: sE.experiment(ID).label(),sE.experiments().get())
assert set(elabels) == set(['eSame','eDifftype','eNotRemote','eNotLocal']), elabels
assertFilesNoSync(sE.experiment('eNotLocal'))
assertFilesSync(sE.experiment('eSame'))
assertFilesNewSync(sE.experiment('eNotRemote'))
assertFilesNewSync(sE.experiment('eDifftype'))
assert sE.experiment('eDifftype').datatype() == 'xnat:mrSessionData'

assertFilesNoSync(sE.experiment('eNotLocal').scan('scan1'))
assertFilesSync(sE.experiment('eSame').scan('scan1'))
assertFilesNewSync(sE.experiment('eNotRemote').scan('scan1'))
assertFilesNewSync(sE.experiment('eDifftype').scan('scan1'))

#SE is good

sNL=p.subject('sNotLocal')
elabels=['eSame3','eDifftype3','eNotLocal3']
assert len(sNL.experiments().get()) == 3
for e in elabels:
    assert sNL.experiment(e).exists() 
    ex=sNL.experiment(e)
    if e=='eDifftype3': assert ex.datatype()=='xnat:crSessionData', ex.datatype()
    else:assert ex.datatype()=='xnat:mrSessionData', ex.datatype()
    assertFilesNoSync(ex)
    assertFilesNoSync(ex.scan('scan1'))

#sNL is good
