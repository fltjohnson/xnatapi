__author__ = 'fjohnson'
from pyxnat import Interface
from xnatapi.xnatapi import getsessioncookiefile,curlcall,surljoin,archive
from tempfile import NamedTemporaryFile
from lxml import etree
from xnatapi.xnatxmlformat import fixup_subject_xml,fixup_project_xml
import sys
import os
import difflib
from StringIO import StringIO

'''Run after loadsrc.py and a sync to verify that the sync worked.
Compare what is found on the site specified with data loaded by loadsrc.py
in the project/ folder.
'''

def err(message):
    print >> sys.stderr, message
    sys.exit(1)

if __name__=='__main__':
    args=sys.argv
    usage='usage: diffsrc.py site user pass IGT_GLIOMA|IGT_GLIOMA2'
    if len(args) != 5: print args;err(usage)
    os.chdir(os.path.dirname(os.path.realpath(__file__)))

    if args[1][-1] == '/': args[1] = args[1][:-1]
    interface = Interface(
        server=args[1],
        user=args[2],
        password=args[3],
        cachedir='/tmp'
    )
    pname=args[4]
    if pname != 'IGT_GLIOMA2' and pname != 'IGT_GLIOMA':
        print args
        err(usage)

    session=getsessioncookiefile(args[1],args[2],args[3])

    server=args[1]

    #assert project metadata and existance
    project=interface.select.project(pname)
    assert project.exists()

    with open('project/%s.xml'%pname) as pmetadata:
        so,se,ret=curlcall(surljoin(server,'data','archive','projects',pname,'?format=xml'),session=session)
        root_remote = fixup_project_xml(etree.fromstring(so))
        root = etree.parse(pmetadata).getroot()

        #We need to remove the project element because it has
        #namespace declarations that cannot be removed and interfere with comparisons on equality
        #I've also had quite a bit of trouble working with the description element.
        #xnat likes to reformat the description.

        proot=etree.Element("Project",ID=root.attrib['ID'],
                            secondary_ID=root.attrib['secondary_ID'])
        for child in root:
            tag = etree.QName(child)
            if tag.localname != 'description':
                proot.append(child)

        proot_remote=etree.Element("Project",ID=root_remote.attrib['ID'],
                                   secondary_ID=root_remote.attrib['secondary_ID'])
        for child in root_remote:
            tag = etree.QName(child)
            if tag.localname != 'description':
                proot_remote.append(child)

        #using stringio preserves newlines
        rs,rrs = StringIO(etree.tostring(proot)).readlines(), StringIO(etree.tostring(proot_remote)).readlines()

        if rs != rrs:
            d = difflib.Differ()
            result = list(d.compare(rrs, rs))
            sys.stdout.writelines(result)

            assert False, "Project metadata differs"
    
    #assert resources
    def assertrez(element):
        assert element.resource('test').exists()
        assert element.resource('test').file('f').exists()
        
    assertrez(project)
    s=project.subject('case01')
    assert s.exists()

    assertrez(s)
    for e in s.experiments():
        assertrez(e)
        for scan in e.scans(): assertrez(scan)

    #assert case01 metadata
    with open('project/case01_%s.xml' % pname) as casexmlf:
        emap={}
        for e in s.experiments(): emap[e.label()]=e.id()

        so,se,ret=curlcall(surljoin(server,'data','archive','projects',
                                    pname,'subjects','case01','?format=xml'),session=session)

        root = fixup_subject_xml(etree.parse(casexmlf).getroot() ,s.id(),project.id(),emap)
        root_remote=fixup_subject_xml(etree.fromstring(so), s.id(),project.id(),emap)

        #check Subject attributes. We need to remove the subject element because it has
        #namespace declarations that cannot be removed and interfere with comparisons on equality

        sroot=etree.Element("Subject",ID=root.attrib['ID'],
                                   project=root.attrib['project'],
                                   label=root.attrib['label'])
        for child in root:
            sroot.append(child)

        sroot_remote=etree.Element("Subject",ID=root_remote.attrib['ID'],
                                   project=root_remote.attrib['project'],
                                   label=root_remote.attrib['label'])
        for child in root_remote:
                sroot_remote.append(child)

        #using stringio preserves newlines
        rs,rrs = StringIO(etree.tostring(root)).readlines(), StringIO(etree.tostring(root_remote)).readlines()
        # open('/root/rs','w').writelines(rs)
        # open('/root/rrs','w').writelines(rrs)

        if rs != rrs:
            d = difflib.Differ()
            result = list(d.compare(rrs, rs))
            sys.stdout.writelines(result)

            assert False, "Subject metadata differs"
