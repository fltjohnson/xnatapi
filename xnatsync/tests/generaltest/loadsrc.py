from pyxnat import Interface
from xnatapi.xnatapi import getsessioncookiefile,curlcall,surljoin,archive
from tempfile import NamedTemporaryFile
from lxml import etree
from xnatapi.xnatxmlformat import fixup_subject_xml,fixup_project_xml
import sys
import os

'''Load a site with publicly available dicom data from xnat central.
The goal of this file is to create a project that has metadata both
on the project level and the subject/experiment level. This provides
a good sync source to test out whether the sync script is working.

After loading data, the sync script should be run and then diffsrc.py
should be used to determine if the sync went off without a hitch.

The data loaded is contained in the project/ folder.
'''

def err(message):
    print >> sys.stderr, message
    sys.exit(1)
    
if __name__=='__main__':
    args=sys.argv
    usage='usage: loadsrc.py siteurl user pass IGT_GLIOMA|IGT_GLIOMA2'
    if len(args) != 5: print args;err(usage)
    os.chdir(os.path.dirname(os.path.realpath(__file__)))

    if args[1][-1] == '/': args[1] = args[1][:-1]
    interface = Interface(
              server=args[1],
              user=args[2],
              password=args[3],
              cachedir='/tmp'
              )
    pname=args[4]
    if pname != 'IGT_GLIOMA2' and pname != 'IGT_GLIOMA':
        print args
        err(usage)

    session=getsessioncookiefile(args[1],args[2],args[3])
    print 'Got auth session.'

    server=args[1]

    project=interface.select.project(pname)
    if project.exists():
        project.delete()
        print 'project deleted'
    project.insert()
    print 'project created'
    
    with open('project/%s.xml'%pname) as pmetadata:
        root=etree.parse(pmetadata).getroot()
        fixup_project_xml(root)
        curlcall(surljoin(server,'data','archive','projects',pname),
                 method='PUT',session=session,stdin=etree.tostring(root))
        print 'metadata for projet stored'
    archive('project/sdicoms.zip',server,pname,session)
    print 'dicom archived'

    f=NamedTemporaryFile('wf')
    f.write('f'*1024)
    f.flush()
    
    
    project.subject('case01').insert()
    case01=project.subject('case01')
    case01.experiment('case01e').insert()
    case01.experiment('case01e').scan('01').insert()
    case01.experiment('case01e').scan('01').resource('nifti').file('f').insert(f.name)
    
    project.subject('case02').insert()
    project.subject('case03').insert()

    def addResources(project):
        def addrez(element):
            element.resource('test').file('f').insert(f.name)
        addrez(project)
        s=project.subject('case01')
        addrez(s)
        for e in s.experiments():
            addrez(e)
            for s in e.scans(): addrez(s)
    addResources(project)

    print 'Subjects created'
    #fix up metadata
    #need project id, subject id, map of experiment labels to ids. 
    
    with open('project/case01_%s.xml' % pname) as casexmlf:
        root=etree.parse(casexmlf).getroot()
        s=project.subject('case01')
        emap={}
        for e in s.experiments(): emap[e.label()]=e.id()
        fixup_subject_xml(root,s.id(),project.id(),emap)
        curlcall(surljoin(server,case01._uri),method='PUT',
                 session=session,stdin=etree.tostring(root))
    
    print 'case01 subject metadata uploaded'

    project=interface.select.project('NotInSR')
    if not project.exists(): project.insert()

    print 'NotInSR project created'
