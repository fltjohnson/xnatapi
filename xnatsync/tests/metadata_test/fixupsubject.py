from xnatapi import getsessioncookiefile,getXMLTree,getSubjectFixupInfo
from xnatxmlformat import fixup_subject_xml
from lxml import etree
import sys

'''Fix up a subject's xml. Useful for debugging'''
path=sys.argv[1] #url to subject
components=path.split('/',4)
url=components[0]+'//'+components[2]+'/'+components[3]
session=getsessioncookiefile(url)
subject_tree=getXMLTree(path,session)

fixup_info=getSubjectFixupInfo(path,session)
fixup_subject_xml(subject_tree,*fixup_info)
print etree.tostring(subject_tree,pretty_print=True)


