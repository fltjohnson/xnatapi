from lxml import etree
import os
from xnatxmlformat import fixup_subject_xml,fixup_project_xml

pxml=open(os.path.join(os.getcwd(),'project.xml'))
sxml=open(os.path.join(os.getcwd(),'subject.xml'))

pxml_expected=open(os.path.join(os.getcwd(),'project_expected.xml'))
sxml_expected=open(os.path.join(os.getcwd(),'subject_expected.xml'))

root=etree.parse(pxml).getroot()
fixup_project_xml(root)
assert etree.tostring(root) == etree.tostring(etree.parse(pxml_expected).getroot())

root=etree.parse(sxml).getroot()
fixup_subject_xml(root,'modsubid','modprojid',{'mrl':'111'})
assert etree.tostring(root) == etree.tostring(etree.parse(sxml_expected).getroot())

print 'EVERYTHING OK'
