######### XNAT bugs encountered
# 1. Deleting a resource only deletes the resource's files and not the actual resource.
# 2. If you specify say ... https://localhost:4002/spred/data/archive/projects/xnatav/files
# as a target for a file upload and xnatav/resources/X already exists, your upload will not
# end up in the NO LABEL resource but will end up in X instead. Tested only on a project.
#3. If you end up uploading files to a url such as
#https://localhost:4002/spred/data/archive/projects/stestingop/files/files
#(note the extraneous '/files') and there exist no resources a null resource will
#be created, but because of the incorrect '/files' suffix any files placed in the null
#resource will be unrecoverable and xnat will complain about their being no catalog
#entries for the requested files
#4. If you try and GET this url https://localhost:4002/spred/data/archive/projects/%s/
# (yes, include %s), xnat will not respond with any error output,
# it will respond with an error code (400) however.
#5. Doing a PUT with an xml document to a url pointing to a scan does not work. XNAT
#responds with an internal server error. So a url of https://.../experiments/exp/scans/1
#does not work for PUTs of xml docs

'''This module contains tests for the xnatsync program.
Be forewarned, some of these tests take an extremely long time to finish due to the
extensive project deletion/creation steps.'''

from xnatapi import getsessioncookiefile, syncProject, syncExperiment, syncSubject,\
    putProject, deleteItem, surljoin, qcurlcall, getJSON, syncFiles, xnatApiException,\
    putSubject, putExperiment, putScan, getpassfile
import re
import os
import unittest
import inspect
from lxml import etree
from tempfile import NamedTemporaryFile

src_url = 'https://localhost:4002/spred'
slogin = 'admin'
spassw = 'admin'

dst_url = 'https://localhost:4004/spred'
dlogin = 'admin'
dpassw = 'admin'

project = 'sytesting'
s1 = getsessioncookiefile(src_url,slogin,spassw)
s2 = getsessioncookiefile(dst_url,dlogin,dpassw)

sr_url = 'https://spred.braincode.ca/spred/'
sr_cred = getpassfile('a','a')

class ResourceTests:
    '''Not actually a testing class, just helper functions for testing'''
    
    #set up files for syncing
    same =  NamedTemporaryFile()
    diff = NamedTemporaryFile()
    nonexist = NamedTemporaryFile()

    same.write('ffff')
    diff.write('bbbb')
    nonexist.write('aaaa')

    for f in [same, diff, nonexist]:
        f.flush()
        os.fsync(f.fileno())

    def change_diff(self, text):
        '''Change the contents of the difference file'''
        self.diff.seek(0)
        self.diff.truncate(0)
        self.diff.write(text)
        self.diff.flush()
        os.fsync(self.diff.fileno())

    def assert_resource(self, rurl):
        '''Check that a resource was sync'd properly'''

        e = qcurlcall(surljoin(rurl, 'files', os.path.basename(ResourceTests.same.name)), session=s2)[0]
        assert e == 'ffff', e
        e = qcurlcall(surljoin(rurl, 'files', os.path.basename(ResourceTests.diff.name)), session=s2)[0]
        assert e == 'bbbb', e
        e = qcurlcall(surljoin(rurl, 'files', os.path.basename(ResourceTests.nonexist.name)), session=s2)[0]
        assert e == 'aaaa', e

    def setup_local_resource(self, url, isnull):

        '''Set up a test resource to sync.
        This resource may be a null resource (resource with empty label)
        This resource contains three files which test what happens when ...
         1. A file exists locally and remotely and is the same file (same name too)
         2. A file exists that has the same name as a file on the remote side but differs in content
         3. A file exists locally does not exist on the remote side.
'''
        url = surljoin(src_url,url)
        if not isnull: res_url = surljoin(url,'resources', 'A')
        else: res_url = url

        qcurlcall(res_url, files=[ResourceTests.same.name,
                                  ResourceTests.diff.name,
                                  ResourceTests.nonexist.name], session=s1)
   

    def setup_url(self, extra_url_params=None):
        '''Create a project,scan,experiment,subject on both the src and dst'''
        url = self.url if not extra_url_params else self.url + extra_url_params
        qcurlcall(surljoin(src_url, url), method='PUT', session=s1)
        #should throw exception if object not created
        qcurlcall(surljoin(src_url, url), session= s1)

        qcurlcall(surljoin(dst_url, url), method='PUT', session=s2)
        #should throw exception if object not created
        qcurlcall(surljoin(dst_url, url), session= s2)

    def teardown_url(self):
        '''Delete the project,scan,experiment,subject created with setup_url on both the src and dst'''
        sitem = surljoin(src_url, self.url)
        ditem = surljoin(dst_url, self.url)

        for i,s in [(sitem,s1), (ditem,s2)]:
            deleteItem(i,s)

        #check that an exception is raised because the url should have been deleted
        try:
            qcurlcall(i, session=s)
        except xnatApiException:
            return
        assert False

    #This next set of tests are used in aggregate to test out file copying capabilities
    #START

    def test_uploads_via_zips(self):
        '''Test uploads where zips are used. This happens when >=10 files are
           present to be syncd'''

        files = [NamedTemporaryFile() for i in xrange(0,15)]
        fpaths = map(lambda f: f.name, files)
        for f in files:
            f.write("100")
            f.flush()
            os.fsync(f.fileno())

        local_url = surljoin(src_url, self.url,'resources/A')
        qcurlcall(local_url,session=s1, files=fpaths)
        syncFiles(src_url, dst_url, self.url, s1, s2)

        for f in files:
            remote_url = surljoin(dst_url, self.url, 'resources/A/files/%s'
                                           % os.path.basename(f.name))
            v = qcurlcall(remote_url, session=s2)[0]
            assert v == "100", v

    def test_sync_resource_to_remote_nonexist(self):
        '''Test copying a resource where this resource dne on the remote side'''

        self.setup_local_resource(self.url, isnull=False)
        syncFiles(src_url,dst_url,self.url,s1,s2)

        remote_res_url = surljoin(dst_url,self.url,'resources','A')
        self.assert_resource(remote_res_url)

    def test_sync_null_resource_to_remote_nonexist(self):
        '''Test copying a null resource where this resource dne on the remote side'''

        self.setup_local_resource(self.url, isnull=True)

        syncFiles(src_url,dst_url,self.url,s1,s2)

        remote_url = surljoin(dst_url,self.url, 'resources')
        for label,id in getJSON(remote_url, 'label', 'xnat_abstractresource_id', session=s2):
            if label == '': break
        assert label == '', 'Null resource not found'

        #reference the null resource with its ID
        remote_res_url = surljoin(remote_url,id)
        self.assert_resource(remote_res_url)

    def test_merge_named_resource(self):
        '''Test syncing a resource that exists on both sides.
        This resource should have a file that exists on both sides that is the same,
        one that is different, and one does that does not exist remotely'''

        self.setup_local_resource(self.url, isnull=False)
        rurl = surljoin(dst_url, self.url, 'resources/A')

        self.change_diff('changed')
        try:
            qcurlcall(rurl,session=s2, files=[ResourceTests.same.name,
                                              ResourceTests.diff.name])
        finally:
            self.change_diff('bbbb')

        syncFiles(src_url,dst_url,self.url,s1,s2)
        self.assert_resource(rurl)

    def test_sync_null_res_to_remote_with_named_res(self):
        '''Sync a null resource where the remote dst already has a named
        resource but no null resource'''

        self.setup_local_resource(self.url, isnull=True)
        remote_named_url = surljoin(dst_url, self.url, 'resources/A')
        qcurlcall(remote_named_url,session=s2, files=[ResourceTests.same.name])
        syncFiles(src_url,dst_url,self.url,s1,s2)

        remote_null_url = surljoin(dst_url, self.url, 'resources/_nolabel')
        self.assert_resource(remote_null_url)

    def test_sync_null_resource(self):
        '''Test syncing a null resource that exists on both ends'''

        self.setup_local_resource(self.url, isnull=True)
        rurl = surljoin(dst_url, self.url)

        self.change_diff('changed')
        try:
            qcurlcall(rurl,session=s2, files=[ResourceTests.same.name,
                                              ResourceTests.diff.name])
        finally:
            self.change_diff('bbbb')

        syncFiles(src_url,dst_url,self.url,s1,s2)
        self.assert_resource(rurl)

    def test_merge_null_resource(self):
        '''Do a sync of a null resource when the dst already has a named resource
        and a _nolabel resource'''

        self.setup_local_resource(self.url, isnull=True)
        remote_named_url = surljoin(dst_url, self.url, 'resources/A')
        qcurlcall(remote_named_url,session=s2, files=[ResourceTests.same.name])

        remote_null_url = surljoin(dst_url, self.url, 'resources/_nolabel')
        self.change_diff('changed')
        try:
            qcurlcall(remote_null_url,session=s2, files=[ResourceTests.same.name,
                                                         ResourceTests.diff.name])
        finally:
            self.change_diff('bbbb')

        syncFiles(src_url,dst_url,self.url,s1,s2)
        self.assert_resource(remote_null_url)

    def test_null_with_named_resource(self):
        '''Sync a null resource (not a _nolabel) that exists locally and remotely when a
        named resource also exists remotely'''

        self.setup_local_resource(self.url, isnull=True)
        remote_null_url = surljoin(dst_url, self.url)
        self.change_diff('changed')
        try:
            qcurlcall(remote_null_url,session=s2, files=[ResourceTests.same.name,
                                                         ResourceTests.diff.name])
        finally:
            self.change_diff('bbbb')

        remote_named_url = surljoin(dst_url, self.url, 'resources/A')
        qcurlcall(remote_named_url,session=s2, files=[ResourceTests.same.name])

        syncFiles(src_url,dst_url,self.url,s1,s2)
        self.assert_resource(remote_null_url)

    def test_local_null_and_named_res_sync(self):
        '''Test syncing a local named resource and a null resource to a dst with no resources'''

        self.setup_local_resource(self.url, isnull=True)
        self.setup_local_resource(self.url, isnull=False)

        syncFiles(src_url,dst_url,self.url,s1,s2)
        self.assert_resource(surljoin(dst_url,self.url,'resources/A'))

        remote_url = surljoin(dst_url,self.url, 'resources')
        for label,id in getJSON(remote_url, 'label', 'xnat_abstractresource_id', session=s2):
            if label == '': break
        assert label == '', 'Null resource not found'

        #reference the null resource with its ID
        remote_res_url = surljoin(remote_url,id)
        self.assert_resource(remote_res_url)

    def test_resource_sync(self, url, extra_url_param=None):
        '''url is a project,subject,experiment,scan url used for resource testing
        i.e url = /data/archive/projects/pname
        extra_url_param is a paramter string to add to the end of the url
        ie ?xsiType=xnat:MRSession
        '''

        self.url = url
        for name,f in inspect.getmembers(self, inspect.ismethod):
            if name.startswith('test_') and name != 'test_resource_sync':
                self.setup_url(extra_url_param) #create the item pointed to by url
                f()
                self.teardown_url() #tear it down

resource_tests= ResourceTests()

class SetUpTearDown(unittest.TestCase):
    '''Basic setup that all test cases probably need'''

    def setUp(self):
        if project in getJSON(surljoin(src_url,'data','archive','projects'),'ID', session=s1):
            deleteItem(surljoin(src_url,'data','archive','projects',project),s1)
        if project in getJSON(surljoin(dst_url,'data','archive','projects'),'ID', session=s2):
            deleteItem(surljoin(dst_url,'data','archive','projects',project),s2)
        putProject(src_url,project,s1)
        putProject(dst_url,project,s2)

    def tearDown(self):
        deleteItem(surljoin(src_url,'data','archive','projects',project),s1)
        deleteItem(surljoin(dst_url,'data','archive','projects',project),s2)

class TestStructural(SetUpTearDown):
    '''Test basic syncing functionality here. Metadata syncing + SR interactions are not tested'''

    def setUp(self):
        if project in getJSON(surljoin(src_url,'data','archive','projects'),'ID', session=s1):
            deleteItem(surljoin(src_url,'data','archive','projects',project),s1)
        if project in getJSON(surljoin(dst_url,'data','archive','projects'),'ID', session=s2):
            deleteItem(surljoin(dst_url,'data','archive','projects',project),s2)
        putProject(src_url,project,s1)
        putProject(dst_url,project,s2)

    def tearDown(self):
        deleteItem(surljoin(src_url,'data','archive','projects',project),s1)
        deleteItem(surljoin(dst_url,'data','archive','projects',project),s2)

    def test_session_cookie_file(self):
        '''Test getting a session'''
        file=getsessioncookiefile(src_url,slogin,spassw)
        with open(file.name) as cookie_file:
            assert re.search('JSESSIONID\t([0-9]|[A-Z]){32}',cookie_file.read())
        file.close()

    def test_sync_project_nonexist_local(self ):
        '''Test syncing a project to a remote host that when the local host does not have the
          project created. This test should fail as the local project must exist'''
        deleteItem(surljoin(src_url,'data','archive','projects',project),s1)
        assert syncProject(src_url,dst_url,project,s1,s2,None,None,
                    bypass_sr=True,delete_diff_exp=True,auto_create_subjects=True) == 1


    def test_sync_project_nonexist_remote(self ):
        '''Test syncing a project to a remote host that does not have the project created.
           This test should fail as the remote project must exist'''

        deleteItem(surljoin(dst_url,'data','archive','projects',project),s2)
        assert syncProject(src_url,dst_url,project,s1,s2,None,None,
                    bypass_sr=True,delete_diff_exp=True,auto_create_subjects=True) == 3

    def test_sync_project_nonexist_remote_override(self):
        '''Test creating remote projects automatically'''
        deleteItem(surljoin(dst_url,'data','archive','projects',project),s2)
        assert syncProject(src_url,dst_url,project,s1,s2,None,None,
                    bypass_sr=True,auto_create_projects=True) == 0
        assert project in getJSON(surljoin(dst_url,'data','archive','projects'),'ID',session=s2)

    def test_sync_subject_create(self):
        '''Test that subjects are created automatically if they dont exist remotely'''

        putSubject(surljoin(src_url, 'data/archive/projects', project), 'tsub', session=s1)
        syncProject(src_url,dst_url,project,s1,s2,None,None,bypass_sr=True)
        out = qcurlcall(surljoin(dst_url, 'data/archive/projects', project, 'subjects'), session=s2)[0]
        subjects = getJSON(surljoin(dst_url, 'data/archive/projects', project, 'subjects'), 'label', session=s2)
        assert 'tsub' in subjects, out

    def test_sync_subject_no_create(self):
        '''Test that subjects are not created automatically if they don't exist remotely'''

        putSubject(surljoin(src_url, 'data/archive/projects', project), 'tsub', session=s1)
        syncProject(src_url,dst_url,project,s1,s2,None,None,bypass_sr=True,auto_create_subjects=False)
        out = qcurlcall(surljoin(dst_url, 'data/archive/projects', project, 'subjects'), session=s2)[0]
        subjects = getJSON(surljoin(dst_url, 'data/archive/projects', project, 'subjects'), 'label', session=s2)
        assert 'tsub' not in subjects, out

    def test_create_experiment_type(self):
        '''Test creating an experiment with a type other than xnat:MRSession'''

        surl = putSubject(surljoin(src_url, 'data/archive/projects', project), 'tsub', session=s1)
        putExperiment(surl,'texp',etype='xnat:EEGSession',session=s1)
        out = qcurlcall(surljoin(surl,'experiments?format=JSON'),session=s1)[0]
        types = getJSON(surljoin(surl,'experiments'), 'xsiType', session=s1)
        assert 'xnat:eegSessionData' in types, out

    def test_sync_experiment_create(self):
        '''Test that experiments are automatically created when they don't exist remotely.'''

        surl = putSubject(surljoin(src_url, 'data/archive/projects', project), 'tsub', session=s1)
        putExperiment(surl,'texp',session=s1)
        syncProject(src_url,dst_url,project,s1,s2,None,None,bypass_sr=True)
        r_exp = surljoin(dst_url, 'data/archive/projects', project,
                                 'subjects/tsub/experiments')
        out = qcurlcall(r_exp+'?format=JSON', session=s2)[0]
        experiments = getJSON(r_exp,'label',session=s2)
        assert 'texp' in experiments, out

    def test_sync_experiment_delete_diff_type(self):
        '''Test that experiments that exist remotely but have a differing type are deleted
        and replaced with the proper type.'''

        surl = putSubject(surljoin(src_url, 'data/archive/projects', project), 'tsub', session=s1)
        putExperiment(surl,'texp',session=s1)
        r_surl = putSubject(surljoin(dst_url, 'data/archive/projects', project), 'tsub', session=s2)
        putExperiment(r_surl,'texp',etype='xnat:EEGSession',session=s2)

        syncProject(src_url,dst_url,project,s1,s2,None,None,bypass_sr=True)
        r_exp = surljoin(dst_url, 'data/archive/projects', project,
                                 'subjects/tsub/experiments')
        out = qcurlcall(r_exp+'?format=JSON', session=s2)[0]
        experiments = getJSON(r_exp,'xsiType',session=s2)
        assert 'xnat:mrSessionData' in experiments, out

    def test_sync_experiment_no_delete_diff_type(self):
        '''Test that experiments that exist remotely but have a differing type are not
        deletedand replaced with the proper type when the delete_diff_exp option is False.'''

        surl = putSubject(surljoin(src_url, 'data/archive/projects', project), 'tsub', session=s1)
        putExperiment(surl,'texp',session=s1)
        r_surl = putSubject(surljoin(dst_url, 'data/archive/projects', project), 'tsub', session=s2)
        putExperiment(r_surl,'texp',etype='xnat:EEGSession',session=s2)

        syncProject(src_url,dst_url,project,s1,s2,None,None,bypass_sr=True,delete_diff_exp=False)
        r_exp = surljoin(dst_url, 'data/archive/projects', project,
                                 'subjects/tsub/experiments')
        out = qcurlcall(r_exp+'?format=JSON', session=s2)[0]
        experiments = getJSON(r_exp,'xsiType',session=s2)
        assert 'xnat:eegSessionData' in experiments, out

    def test_sync_project(self):
        '''Test syncing files belonging to projects
         i.e test syncing files in projects/resources/X '''

        resource_tests.test_resource_sync('data/archive/projects/%s'%project)

    def test_sync_subject(self):
        '''Test syncing files belonging to subjects
         i.e test syncing files in subjects/sname/resources/X '''

        putSubject(surljoin(src_url,'data/archive/projects/%s'%project),'tsubj',s1)
        putSubject(surljoin(dst_url,'data/archive/projects/%s'%project),'tsubj',s2)
        resource_tests.test_resource_sync('data/archive/projects/%s/subjects/%s'%(project,'tsubj'))

    def test_sync_experiment(self):
        '''Test syncing an experiment of type MRSession'''

        surl1 = putSubject(surljoin(src_url,'data/archive/projects/%s'%project),'tsubj',s1)
        surl2 = putSubject(surljoin(dst_url,'data/archive/projects/%s'%project),'tsubj',s2)

        resource_tests.test_resource_sync('data/archive/projects/%s/subjects/%s/experiments/%s'%
                                          (project,'tsubj','etest'),
                                          '?xsiType=xnat:MRSession')

    def test_sync_experiment_eeg(self):
        '''Test syncing an experiment of type EEGSession'''

        surl1 = putSubject(surljoin(src_url,'data/archive/projects/%s'%project),'tsubj',s1)
        surl2 = putSubject(surljoin(dst_url,'data/archive/projects/%s'%project),'tsubj',s2)

        resource_tests.test_resource_sync('data/archive/projects/%s/subjects/%s/experiments/%s'%
                                          (project,'tsubj','etest'),
                                          '?xsiType=xnat:EEGSession')
    def test_sync_scan(self):
        '''Test syncing mrScanData'''

        surl1 = putSubject(surljoin(src_url,'data/archive/projects/%s'%project),'tsubj',s1)
        surl2 = putSubject(surljoin(dst_url,'data/archive/projects/%s'%project),'tsubj',s2)
        eurl1 = putExperiment(surl1,'etest','xnat:MRSession',s1)
        eurl2 = putExperiment(surl2,'etest','xnat:MRSession',s2)

        resource_tests.test_resource_sync('data/archive/projects/%s/subjects/%s/experiments/%s/scans/%s'%
                                          (project,'tsubj','etest','tscan'),
                                          '?xsiType=xnat:mrScanData')

    def test_create_scan_type(self):
        '''Test creating a scan of a specific type'''

        surl1 = putSubject(surljoin(src_url,'data/archive/projects/%s'%project),'tsubj',s1)
        eurl1 = putExperiment(surl1,'etest','xnat:EEGSession',s1)
        scan_url = putScan(eurl1, 'tscan', s1, xsiType='xnat:eegScanData')
        out = qcurlcall(surljoin(eurl1,'scans'),session=s1)[0]
        types = getJSON(surljoin(eurl1,'scans'), 'xsiType', session=s1)
        assert 'xnat:eegScanData' in types

    def test_sync_eeg_scan(self):
        '''Test syncing eegScanData'''

        surl1 = putSubject(surljoin(src_url,'data/archive/projects/%s'%project),'tsubj',s1)
        surl2 = putSubject(surljoin(dst_url,'data/archive/projects/%s'%project),'tsubj',s2)
        eurl1 = putExperiment(surl1,'etest','xnat:EEGSession',s1)
        eurl2 = putExperiment(surl2,'etest','xnat:EEGSession',s2)

        resource_tests.test_resource_sync('data/archive/projects/%s/subjects/%s/experiments/%s/scans/%s'%
                                          (project,'tsubj','etest','tscan'),
                                          '?xsiType=xnat:eegScanData')

    def test_upload_empty_files(self):
        '''curl will not download 0 byte files. 0 byte files should be sync'd properly'''

        empty_file = NamedTemporaryFile()
        local_url = surljoin(src_url,'data/archive/projects/%s/resources/A' % project)
        qcurlcall(local_url,session=s1, files=[empty_file.name])
        syncFiles(src_url, dst_url, 'data/archive/projects/%s' % project, s1, s2)

        #This resource should not have been created because empty files are not sync'd
        rez = getJSON(surljoin(dst_url,'data/archive/projects/%s/resources' % project), 'label', session=s2)
        assert 'A' not in rez, rez

    def test_upload_empty_zip(self):
        '''Test syncing > 10 files that are emtpy. This invokes a zip upload'''

        files = [NamedTemporaryFile() for i in xrange(0,15)]
        fpaths = map(lambda f: f.name, files)
        local_url = surljoin(src_url,'data/archive/projects/%s/resources/A' % project)
        qcurlcall(local_url,session=s1, files=fpaths)

        syncFiles(src_url, dst_url, 'data/archive/projects/%s' % project, s1, s2)

        #This resource should not have been created because empty files are not sync'd
        rez = getJSON(surljoin(dst_url,'data/archive/projects/%s/resources' % project), 'label', session=s2)
        assert 'A' not in rez, rez

    def test_snapshot_uploads(self):
        '''Test an upload of snapshot files'''

        surl1 = putSubject(surljoin(src_url,'data/archive/projects/%s'%project),'tsubj',s1)
        surl2 = putSubject(surljoin(dst_url,'data/archive/projects/%s'%project),'tsubj',s2)
        eurl1 = putExperiment(surl1,'etest','xnat:MRSession',s1)
        eurl2 = putExperiment(surl2,'etest','xnat:MRSession',s2)
        scurl1 = putScan(eurl1, 'tscan', s1, xsiType='xnat:mrScanData')
        scurl2 = putScan(eurl2, 'tscan', s2, xsiType='xnat:mrScanData')
        snapshot_url1 = surljoin(scurl1,'resources/SNAPSHOTS')

        #The uploaded files don't need to be real gifs
        qcurlcall(snapshot_url1, session=s1, files=[ResourceTests.same.name, ResourceTests.diff.name])

        scan_url = 'data/archive/projects/%s/subjects/tsubj/experiments/etest/scans/tscan' % project
        syncFiles(src_url, dst_url, scan_url, s1, s2)

        xml = qcurlcall(surljoin(dst_url,scan_url,'resources','SNAPSHOTS'),session=s2)[0]
        doc = etree.fromstring(xml)
        entries = doc.xpath('//cat:entry', namespaces=doc.nsmap)
        assert len(entries) != 0, len(entries)

        for e in entries:
            v = e.attrib['content']
            if e.attrib['URI'].endswith('_t.gif'):
                assert v == 'THUMBNAIL', v
            else:
                assert v == 'ORIGINAL', v

class TestMetaData(SetUpTearDown):

    def test_project_metadata(self):
        '''Test syncing project metadata'''

        #modify src project metadata
        xml=qcurlcall(surljoin(src_url, 'data/archive/projects/%s?format=xml'%project),session=s1)[0]
        doc = etree.fromstring(xml)

        description = etree.Element("{http://nrg.wustl.edu/xnat}description", nsmap=doc.nsmap)
        description.text = "testdescription"
        doc.append(description)

        keywords = etree.Element("{http://nrg.wustl.edu/xnat}keywords", nsmap=doc.nsmap)
        keywords.text = 'testkeyword'
        doc.append(keywords)

        qcurlcall(surljoin(src_url, 'data/archive/projects/%s'%project), method='PUT',
                  stdin=etree.tostring(doc, pretty_print=True), session=s1)

        assert syncProject(src_url,dst_url,project,s1,s2,None,None,bypass_sr=True) == 0

        #check dst project metadata
        xml=qcurlcall(surljoin(dst_url, 'data/archive/projects/%s?format=xml'%project),session=s2)[0]
        doc2 = etree.fromstring(xml)
        elements_to_find = ['{http://nrg.wustl.edu/xnat}description',
                            "{http://nrg.wustl.edu/xnat}keywords"]
        for child in doc2:
            if child.tag == '{http://nrg.wustl.edu/xnat}description':
                if child.text == 'testdescription':
                    elements_to_find.remove(child.tag)
            elif child.tag == "{http://nrg.wustl.edu/xnat}keywords":
                if child.text == 'testkeyword':
                    elements_to_find.remove(child.tag)


        assert not elements_to_find, "\nElements not found %s\nsrc %s \ndst %s" % \
                                     (elements_to_find,
                                      etree.tostring(doc, pretty_print=True),
                                      etree.tostring(doc2, pretty_print=True))

    def test_subject_metadata(self):
        '''Test syncing subject metadata (sync demographics)'''

        surl1 = putSubject(surljoin(src_url,'data/archive/projects/%s'%project),'tsubj',s1)
        xml=qcurlcall(surl1+'?format=xml',session=s1)[0]
        doc = etree.fromstring(xml)

        demo = etree.Element('{http://nrg.wustl.edu/xnat}demographics')
        for k in doc.nsmap:
            if doc.nsmap[k] == 'http://nrg.wustl.edu/xnat':
                prefix = k
                break
        xsi_attrib = '%s:demographicData' % prefix
        demo.attrib["{http://www.w3.org/2001/XMLSchema-instance}type"] = xsi_attrib

        values =  [('age','12'), ('gender', 'female'), ('handedness', 'right'),
                        ('education', '15'), ('race', 'd'), ('ethnicity', 'a'),
                        ('weight', '5.0'), ('height', '4.0')]

        for tag, v in values:
            e = etree.Element("{http://nrg.wustl.edu/xnat}"+tag, nsmap=doc.nsmap)
            e.text = v
            demo.append(e)
        doc.append(demo)

        qcurlcall(surl1, method='PUT', stdin=etree.tostring(doc, pretty_print=True), session=s1)
        assert syncProject(src_url,dst_url,project,s1,s2,None,None,bypass_sr=True) == 0

        xml = qcurlcall(surljoin(dst_url,'data/archive/projects/%s/subjects/tsubj?format=xml'%project),session=s2)[0]
        doc2 = etree.fromstring(xml)

        for c in doc2:
            if c.tag == "{http://nrg.wustl.edu/xnat}demographics":
                demographics = c
                for dc in demographics:
                    if not isinstance(dc.tag, str): continue #skip hidden comments
                    tag_and_val = (dc.tag.replace("{http://nrg.wustl.edu/xnat}",''), dc.text)
                    if tag_and_val in values:
                        values.remove(tag_and_val)

        assert not values, "\nDemographic elements not found: %s\n dst:%s" % (values, etree.tostring(doc2))

    def test_experiment_metadata(self):
        '''Test syncing scan metadata'''

        surl1 = putSubject(surljoin(src_url,'data/archive/projects/%s'%project),'tsubj',s1)
        eurl1 = putExperiment(surl1, 'etest', session=s1)

        #modify src experiment
        xml = qcurlcall(eurl1+'?format=xml',session=s1)[0]
        doc = etree.fromstring(xml)

        values = [('date', '2009-03-03'), ('note', 'notes'), ('acquisition_site', 'kol'),
            ('scanner', 'some_scanner'), ('operator', 'some_op'), ('coil', 'some_coil'),
            ('marker', 'some_marker'), ('stabilization', 'stabilizer')]

        for tag, v in values:
            e = etree.Element("{http://nrg.wustl.edu/xnat}"+tag, nsmap=doc.nsmap)
            e.text = v
            doc.append(e)

        qcurlcall(eurl1, method='PUT', stdin=etree.tostring(doc, pretty_print=True), session=s1)
        assert syncProject(src_url,dst_url,project,s1,s2,None,None,bypass_sr=True) == 0

        #verify that modifications were sync'd
        xml = qcurlcall(eurl1.replace(src_url, dst_url) + '?format=xml', session=s2)[0]
        doc = etree.fromstring(xml)

        for c in doc:
            if not isinstance(c.tag, str): continue #skip hidden comments
            tag_and_val = (c.tag.replace("{http://nrg.wustl.edu/xnat}",''), c.text)
            if tag_and_val in values:
                values.remove(tag_and_val)

        assert not values, "\nExperiment elements not found: %s\n dst:%s" % (values, etree.tostring(doc))

    def test_scan_metadata(self):
        '''Test syncing scan metadata'''

        surl1 = putSubject(surljoin(src_url,'data/archive/projects/%s'%project),'tsubj',s1)
        eurl1 = putExperiment(surl1, 'etest', session=s1)
        scanurl1 = putScan(eurl1, 'testscan', stype='nifti', session=s1)

        #modify src scan
        #use the experiment url instead of the scanurl because of an xnat bug that
        #prevents us from sending xml documents to the scan url directly.
        xml = qcurlcall(eurl1+'?format=xml',session=s1)[0]
        doc = etree.fromstring(xml)

        for c in doc:
            if c.tag == "{http://nrg.wustl.edu/xnat}scans":
                scan = c[0]
                for e in scan:
                    if 'hidden_fields' in e.text:
                        scan.remove(e)
                    if e.tag == "{http://nrg.wustl.edu/xnat}quality":
                        e.text = 'questionable'

        values = [('quality', 'questionable'), ('note', 'these are the notes')]
        for tag, v in values[1:]:
            e = etree.Element("{http://nrg.wustl.edu/xnat}"+tag, nsmap=doc.nsmap)
            e.text = v
            scan.append(e)

        #Do the PUT directly to the experiment and not the scan because of the mentioned bug
        qcurlcall(eurl1, method='PUT', stdin=etree.tostring(doc, pretty_print=True), session=s1)
        xml = qcurlcall(eurl1+'?format=xml', method='GET', session=s1)[0]

        assert syncProject(src_url,dst_url,project,s1,s2,None,None,bypass_sr=True) == 0

        #verify that the modifications were sync'd
        xml = qcurlcall(scanurl1.replace(src_url, dst_url) + '?format=xml', session=s2)[0]
        doc = etree.fromstring(xml)

        for c in doc:
            if not isinstance(c.tag, str): continue #skip hidden comments
            tag_and_val = (c.tag.replace("{http://nrg.wustl.edu/xnat}",''), c.text)
            if tag_and_val in values:
                values.remove(tag_and_val)

        assert not values, "\nScan elements not found: %s\n dst:%s" % (values, etree.tostring(doc))

        scan_type = doc.attrib['type']
        assert scan_type == 'nifti', 'Was %s, should be nifti' % scan_type

class TestSubjectRegistry(SetUpTearDown):
    '''Test SR interactions.
    For many of these tests you will need to make sure that the project IGT_GLIOMA
    is present in the SR as well as the particular subjects'''

    cgiurl = surljoin(sr_url,'cgi-bin/sr-query?p=%s&s=%s')

    def test_project_not_in_sr(self):
        '''Test that a project is not sync'd when it is not in the subject registry'''

        cgir = qcurlcall(self.cgiurl % ('notinsr', 'na'), passFile=sr_cred.name)[0]
        assert cgir.strip() == '<ret>-2</ret>', 'sr-query returned %s and we wanted <ret>-2</ret>' % cgir

        try:
            putProject(src_url, 'notinsr', session=s1)
            result = syncProject(src_url,dst_url,'notinsr',s1,s2,sr_url,sr_cred.name)

            dst_proj = surljoin(dst_url, 'data', 'archive', 'projects')
            if 'notinsr' in getJSON(dst_proj, 'ID', session=s2):
                deleteItem(surljoin(dst_proj,'notinsr'), session=s2)
                assert False, 'Project was syncd'

            assert result == 2, 'Result was %d, should have been %d' % (result, 2)

        finally:
            deleteItem(surljoin(src_url,'data/archive/projects/notinsr'), session=s1)

    def test_project_sync(self):
        '''Test that syncing a project works correctly when it is in the sr'''

        cgir = qcurlcall(self.cgiurl % (project, 'na'), passFile=sr_cred.name)[0]
        assert cgir.strip() != '<ret>-1</ret>' and cgir.strip() != '<ret>-2</ret>', \
            'sr-query returned %s' % cgir

        p2 = surljoin(dst_url, 'data/archive/projects/%s' % project)
        deleteItem(p2, session=s2)

        result = syncProject(src_url,dst_url,project,s1,s2,sr_url,sr_cred.name,auto_create_projects=True)
        assert result == 0, 'Result was %d, should have been %d' % (result, 0)

        assert project in getJSON(surljoin(dst_url,'data','archive','projects'),'ID',session=s2)

    def test_subject_not_in_sr(self):
        '''Test that a subject not in the sr is not sync'd'''

        cgir = qcurlcall(self.cgiurl % (project, 'tsubj'), passFile=sr_cred.name)[0]
        assert cgir.strip() == '<ret>0</ret>', 'sr-query returned %s and we wanted <ret>0</ret>' % cgir

        putSubject(surljoin(src_url, 'data/archive/projects/%s' % project), 'tsubj', session=s1)
        syncProject(src_url,dst_url,project,s1,s2,sr_url,sr_cred.name)
        assert 'tsubj' not in getJSON(surljoin(dst_url,'data','archive','projects',project,'subjects'),
                                      'label',session=s2)

    def test_subj_not_verified(self):
        '''Test that a non verified subject is not sync'd'''

        cgir = qcurlcall(self.cgiurl % (project, 'tsubj_noverify'), passFile=sr_cred.name)[0]
        assert cgir.strip() == '<ret>1</ret>', 'sr-query returned %s and we wanted <ret>1</ret>' % cgir

        putSubject(surljoin(src_url, 'data/archive/projects/%s' % project), 'tsubj_noverify', session=s1)
        syncProject(src_url,dst_url,project,s1,s2,sr_url,sr_cred.name)
        assert 'tsubj_noverify' not in getJSON(surljoin(dst_url,'data','archive','projects',project,'subjects'),
                                      'label',session=s2)


    def test_subj_verified(self):
        '''Test that a verified subject is sync'd'''

        cgir = qcurlcall(self.cgiurl % (project, 'tsubj_verify'), passFile=sr_cred.name)[0]
        assert cgir.strip() == '<ret>2</ret>', 'sr-query returned %s and we wanted <ret>2</ret>' % cgir

        putSubject(surljoin(src_url, 'data/archive/projects/%s' % project), 'tsubj_verify', session=s1)
        syncProject(src_url,dst_url,project,s1,s2,sr_url,sr_cred.name, auto_create_subjects=True)
        assert 'tsubj_verify' in getJSON(surljoin(dst_url,'data','archive','projects',project,'subjects'),
                                      'label',session=s2)

