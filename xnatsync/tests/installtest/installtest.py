__author__ = 'fjohnson'
import subprocess
import os
import sys
import time
from pyxnat import Interface

'''Test the installation of the xnat sync script

The purpose of this script is two fold. It tests the xnat sync installation procedure
and it also tests that the xnat sync is working correctly after an installation

This script does the following
1. Uses loadsrc.py to load the projects IGT_GLIOMA and IGT_GLIOMA2
into a target xnat url (should be local).
2. Uninstalls any existing xnatsync installation
3. Installs the xnatsync using a config file with a definition for IGT_GLIOMA. This 
triggers a sync of IGT_GLIOMA to the site defined in the config file. 
4. The script sleeps for 5 minutes. Then runs diffsrc.py to determine if the sync
worked.
5. An install is done again, this time with a config definition containing IGT_GLIOMA2.
IGT_GLIOMA2 is syncd.
6. The script sleeps for 5 minutes. Then runs diffsrc.py to determine if the sync
worked.

If no output is presented then the install passed.
'''

def c(command): subprocess.check_call(command.split())
def prnt(string):
    print
    print string
    print
    sys.stdout.flush()

usage='installtest.py sitelocal username password siteremote username password emailpass srlogin srpass'
if len(sys.argv) != 10:
    print usage
    sys.exit(1)

_,sl,slu,slp,sr,sru,srp,ep,SRl,SRp= sys.argv

os.chdir(os.path.dirname(os.path.realpath(__file__)))

prnt('Connecting to site remote and resetting projects ')
#connect to the remote xnat
if sr[-1] == '/': sr = sr[:-1]
interface = Interface(
    server=sr,
    user=sru,
    password=srp,
    cachedir='/tmp'
)

#reset projects
for p in ['IGT_GLIOMA2','IGT_GLIOMA']:
    pobj=interface.select.project(p)
    if pobj.exists(): pobj.delete()
interface.select.project('IGT_GLIOMA').insert()

#  load local xnat with data to sync
c('python ../generaltest/loadsrc.py %s %s %s IGT_GLIOMA'%(sl,slu,slp))
c('python ../generaltest/loadsrc.py %s %s %s IGT_GLIOMA2'%(sl,slu,slp))


#Test a FRESH install
prnt('Running uninstall ')
c('python ../../install/xsync_uninstall.py')

prnt('Writing config ')

with open('config','w') as conf:
    conf.write('''{"IGT_GLIOMA":
      {"email_user":'noreply@research.baycrest.org', "email_pass":'%s',
        "email_host":'smtp.gmail.com', "email_port":'587',
       "site_local":'%s', "site_remote":'%s',
       "local_user":'%s', "local_pass":'%s', "remote_user":'%s', "remote_pass":'%s',
       "tech_email":'fjohnson@research.baycrest.org', "alert_email":'fjohnson@research.baycrest.org'
      },
"_Subject Registry":{
        "sr_url":'https://spred.braincode.ca/spred',
        "sr_login":'%s',
        "sr_pass":'%s'
     },
"_hostname":'nkeypad'
}''' %(ep,sl,sr,slu,slp,sru,srp,SRl,SRp))

prnt('Running install ')
c('python ../../install/xsync_install.py')

prnt('Sleeping for 5 minutes ')
time.sleep(60*5) #wait 5 minutes for the sync to take place

prnt('Running diffsrc.py on IGT_GLIOMA')
c('python ../generaltest/diffsrc.py %s %s %s IGT_GLIOMA' %(sr,sru,srp))

# test an UPDATE install
# here nothing is erased, when the install script is called it will scan the config file for new
# project entries and it will create the necessary init scripts. it will then start the sync for the
# new projects by calling these init scripts

prnt('Adding new project IGT_GLIOMA2 to config')
pobj=interface.select.project('IGT_GLIOMA2')
pobj.insert()

prnt('Writing updated config ')
with open('config','w') as conf:
    conf.write('''{"IGT_GLIOMA2":
      {"email_user":'noreply@research.baycrest.org', "email_pass":'%s',
        "email_host":'smtp.gmail.com', "email_port":'587',
       "site_local":'%s', "site_remote":'%s',
       "local_user":'%s', "local_pass":'%s', "remote_user":'%s', "remote_pass":'%s',
       "tech_email":'fjohnson@research.baycrest.org', "alert_email":'fjohnson@research.baycrest.org'
      }}'''%(ep,sl,sr,slu,slp,sru,srp))

prnt('Running install ')
c('python ../../install/xsync_install.py')

prnt('Sleeping for 5 minutes ')
time.sleep(60*5) #wait 5 minutes for the sync to take place

prnt('Running diffsrc.py on IGT_GLIOMA2')
c('python ../generaltest/diffsrc.py %s %s %s IGT_GLIOMA2' %(sr,sru,srp))
