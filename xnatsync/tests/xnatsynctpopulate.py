from __future__ import with_statement
from xnatapi import *

'''Populate a test xnat project for later syncing.
this test should showcase the proper behavior when files are present 
locally but not remotely, remotely but not locally, and when they exist on both 
sides where they are either the same or they differ. 
run xnatsynct.py after running this file to run the test'''

pname = 'synctest' #project test name

#src
site1='https://localhost:8900/spred/'
site1_u='admin'
site1_p='admin'

#dst
site2='https://localhost:8889/spred/'
site2_u='admin'
site2_p='admin'

with open("a",'w') as f: f.write('1111111')
with open("b",'w') as f: f.write('111')
with open("c",'w') as f: f.write('ccc')
with open("d",'w') as f: f.write('ddd')
with open("e",'w') as f: f.write('eee')
with open("f",'w') as f: f.write('fff')

def putFiles(url,project,session):
    
    if project == 'p1':
        curlcall(url,session=s1s,files=["a","b","c"])
        putResource(url,'rSame',session=s1s)
        target = urljoin(url,'resources','rSame')
        curlcall(`target`,session=s1s,files=["a","b","c"])
        putResource(url,'rNotRemote',session=s1s)
        target = urljoin(url,'resources','rNotRemote')
        curlcall(`target`,session=s1s,files=["e"])
    else:
        curlcall(url,session=s2s,files=["a","b","d"])
        putResource(url,'rSame',session=s2s)
        target = urljoin(url,'resources','rSame')
        curlcall(`target`,session=s2s,files=["a","b","d"])
        putResource(url,'rNotLocal',session=s2s)
        target = urljoin(url,'resources','rNotLocal')
        curlcall(`target`,session=s2s,files=["f"])

def putTestSubject(proj,name,sess,pnum):
    t=putSubject(proj,name,sess)
    print '\tInserted Subject ' + name

    putFiles(t,pnum,sess)
    print '\tInserted test files for: ' + name

    #experiment labels must be unique for an entire project
    #PUTing a duplicate label will move the experiment not
    #create a new one
    if pnum == 'p1' and name=='sExists':
        putTestExperiment(t,'eSame','xnat:mrSessionData',sess,pnum)
        putTestExperiment(t,'eDifftype','xnat:mrSessionData',sess,pnum)
        putTestExperiment(t,'eNotRemote','xnat:mrSessionData',sess,pnum)
    elif pnum == 'p1' and name=='sNotRemote':
        putTestExperiment(t,'eSame2','xnat:mrSessionData',sess,pnum)
        putTestExperiment(t,'eDifftype2','xnat:mrSessionData',sess,pnum)
        putTestExperiment(t,'eNotRemote2','xnat:mrSessionData',sess,pnum)
    elif pnum == 'p2' and name=='sExists':
        putTestExperiment(t,'eSame','xnat:mrSessionData',sess,pnum)
        putTestExperiment(t,'eDifftype','xnat:CRSession',sess,pnum)
        putTestExperiment(t,'eNotLocal','xnat:mrSessionData',sess,pnum)
    elif pnum == 'p2' and name=='sNotLocal':
        putTestExperiment(t,'eSame3','xnat:mrSessionData',sess,pnum)
        putTestExperiment(t,'eDifftype3','xnat:CRSession',sess,pnum)
        putTestExperiment(t,'eNotLocal3','xnat:mrSessionData',sess,pnum)
      
def putTestExperiment(url,name,typee,sess,pnum):
    putExperiment(url,name,typee,sess)
    print '\t\tInserted %s experiment %s' % (typee,name)
    
    exp=surljoin(url,'experiments',name)
    putFiles(exp,pnum,sess)
    print '\t\tInserted test files for: ' + name

    def putImageType(t,exp,name):
        if t == 'reconstructions':
            name='recon1'
            target = putReconstruction(exp,name,sess)
            print 'Inserted reconstruction %s' % name
        elif t == 'scans':
            if (name == 'eDifftype' or name == 'eDifftype3') and pnum == 'p2':
                name='scan1?xsiType=xnat:crScanData'
            else:
                name='scan1'
            putScan(exp,name,sess)        
            target = surljoin(exp,'scans','scan1')
            print '\t\t\tInserted scan %s' % name
        elif t == 'assessors':
            name='assessor1'
            target = putAssessor(exp,name,sess)
            print 'Inserted assessor %s' % name
        putFiles(target,pnum,sess)
        print '\t\t\tInserted test files for: ' + name

    #for x in ['reconstructions','scans','assessors']: 
        #putImageType(x,exp)
    putImageType('scans',exp,name) #ignore reconstructions/assessors


print '#####P1\n'
print 'Getting session for ' + site1
s1s=getauthsession(site1,site1_u,site1_p)
print 'Session: ' + s1s

t = surljoin(site1,'data','archive','projects',pname)

print 'Deleting %s ...' % t,
deleteItem(t,s1s)
print ' Ok!'

putProject(site1,pname,s1s)
print 'Inserted project: ' + pname

putFiles(t,'p1',s1s)
print 'Inserted test files for: ' + t

putTestSubject(t,'sNotRemote',s1s,'p1')
putTestSubject(t,'sExists',s1s,'p1')

########p2
print '#####P2\n'
print 'Getting session for ' + site2
s2s=getauthsession(site2,site2_u,site2_p)
print 'Session: ' + s2s

t = surljoin(site2,'data','archive','projects',pname)

print 'Deleting %s ...' % t,
deleteItem(t,s2s)
print ' Ok!'

with open("b",'w') as f: f.write('888999')
putProject(site2,pname,s2s)
print 'Inserted project: ' + pname

putFiles(t,'p2',s2s)
print 'Inserted test files for: ' + t

putTestSubject(t,'sNotLocal',s2s,'p2')
putTestSubject(t,'sExists',s2s,'p2')










