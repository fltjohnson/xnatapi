import signal
import shlex
import subprocess
import time
from subprocess import Popen as c

'''This allows me to run the remote xnatsync program locally in my pycharm environment'''

def sig(a,b):
    print 'Caught signal %s' % a
    p.communicate("finish")

if __name__ == '__main__':
    signal.signal(signal.SIGTERM,sig)
    signal.signal(signal.SIGINT,sig)
    # p = c('ssh -p 4000 root@localhost python /root/xnatapi/setup.py install'.split())
    # p.wait()
    # p = c(shlex.split('ssh -p 4000 root@localhost "xnatsync.py IGT_GLIOMA2& read; kill $!"'),stdin=subprocess.PIPE)
    p = c(shlex.split('ssh -p 4000 root@localhost '
                      'python "/root/xnatapi/xnatsync/tests/installtest/installtest.py'
                      ' %s %s %s %s %s %s %s %s %s"' %(

        )),stdin=subprocess.PIPE)
    p.wait()

    # while p.poll() == None:
    #     time.sleep(1)

