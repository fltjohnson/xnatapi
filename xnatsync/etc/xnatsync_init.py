#!/usr/bin/env python
# chkconfigex: runlevel(s) start stop
# lower values have higher priorities
# chkconfig: 35 70 80
# description: Starts and stops X

__author__ = 'fjohnson'
'''Template for a chkconfig startup file'''
import os
import fcntl
import sys
import subprocess
import signal
import site
import time

lockf='/var/run/xnatsync_%s.lock'%__project__
usage='start|stop|reset'
preargs=[]
exe='xnatsync.py'
args=[__project__]

if len(sys.argv) < 2:
    print usage
    sys.exit(0)

arg=sys.argv[1]

if arg not in ['stop','start','restart']:
    print usage
    sys.exit(1)

def err(msg,code=1):
    print >> sys.stderr,msg
    sys.exit(code)

def start():
    if os.path.exists(lockf):
        with open(lockf,'w') as lf:
            try:
                fcntl.lockf(lf.fileno(),fcntl.LOCK_EX|fcntl.LOCK_NB)
                fcntl.lockf(lf.fileno(),fcntl.LOCK_UN)
            except IOError as e:
                if e.errno == 11: #lock already in place
                    err("Already started - %s lock file in use" % lockf)
                print lf
                raise e

    # ok, lock file not in use
    # create process to hold onto lock for as long as program runs before exiting
    if os.fork():
        return

    #see: http://www.win.tue.nl/~aeb/linux/lk/lk-10.html
    devnullw=open('/dev/null','w')
    devnullr=open('/dev/null','r')
    os.dup2(devnullr.fileno(),0)
    os.dup2(devnullw.fileno(),1)
    os.dup2(devnullw.fileno(),2)
    os.setsid() #disconnect tty


    with open(lockf,'w') as lf:
        fcntl.lockf(lf.fileno(),fcntl.LOCK_EX|fcntl.LOCK_NB)
        p=subprocess.Popen(preargs+[exe]+args,close_fds=True,cwd='/') #doesn't close fds 0,1,2

        def sigexit(sig,stack):
            p.send_signal(sig)

        signal.signal(signal.SIGINT,sigexit)
        signal.signal(signal.SIGTERM,sigexit)
        signal.signal(signal.SIGQUIT,sigexit)
        p.wait()

def stop():
    '''I didn't want to use lsof, but there are no interfaces to return a struct flock :('''
    p=subprocess.Popen(['lsof','-F','pnl'],stdout=subprocess.PIPE)
    stdout,stderr=p.communicate()
    output=stdout.split('\n')

    found=False
    i = 0
    while i < len(output)-1: #output has single '' last element due to \n by itself.
        if output[i].startswith('p'):
            pid = output[i][1:]
            i+=1
        lockstatus=output[i][1:].strip() #space implies no lock, otherwise a single character denotes the type of lock
        fname=output[i+1][1:]
        i+=2

        if fname == lockf and lockstatus:
            found=True
            break

    if found:
        pid = int(pid)
        print 'Sending SIGTERM to pid:%d' %pid
        os.kill(pid, signal.SIGTERM)
        try:
            while True:
                time.sleep(.5)
                os.kill(pid,0)
        except OSError, e:
            if e.errno != 3:
                raise #OSError: [Errno 3] No such process
    else:
        print'Lockfile not in use, process not running?'

def restart():
    stop()
    start()

if arg == 'start':start()
elif arg == 'stop':stop()
else:restart()
