#!python
import os
import sys
import logging
import signal
import socket
from logging import handlers
from xnatapi.xnatapi import syncProject,getsessioncookiefile, literal_eval, getpassfile
from sysutil import daemonize,email,getFileExclusiveLock,releaseFileLock
from email.Utils import formatdate
from tempfile import NamedTemporaryFile

#define file names for logs
_pmanager_log_name = '/var/log/xnatsync/projectmanager_%s.log'
_tech_log_name = '/var/log/xnatsync/tech_%s.log'
_srkey='_Subject Registry'

def parse_config(options):
    '''Parse and read configuration details for a particular project

    The config file resides in /root/.xnat_sync.conf and has the following format
    {"project1":
      {"email_user":'a', "email_pass":'a', "email_host":'a', "email_port":'a',
       "site_local":'a', "site_remote":'a',
       "local_user":'a', "local_pass":'a', "remote_user":'a', "remote_pass":'a',
       "tech_email":'a', "alert_email":'a'
      }
     "project2":...,
     "sr_url":'https://spred.braincode.ca/spred'
    }'''


    confstr = '''{"project1":
      {"email_user":'a', "email_pass":'a', "email_host":'a', "email_port":'a',
       "site_local":'a', "site_remote":'a',
       "local_user":'a', "local_pass":'a', "remote_user":'a', "remote_pass":'a',
       "tech_email":'a', "alert_email":'a'
      },
     "project2":...,
     "%s":{
        "sr_url":'https://spred.braincode.ca/spred',
        "sr_login":'a',
        "sr_pass":'a'
     },
     "_hostname":'nkeypad'
    }'''%_srkey

    with open(options['conf']) as xsconf:
        try:config = literal_eval(xsconf.read())
        except SyntaxError:
            err("Config file must be of form ...\n%s"%confstr,7)

    if _srkey not in config:
        err("%s must be configured in the config file."%_srkey,7)
    sr = config[_srkey]
    del config[_srkey]

    if not config['_hostname']:
        err('_hostname cannot be empty', 7)
    hostname = config['_hostname']
    del config['_hostname']

    keys = ["email_user","email_pass","email_host","email_port",
            "site_local","site_remote","local_user","local_pass",
            "remote_user","remote_pass","tech_email","alert_email"]

    for project_name in config:
        project = config[project_name]
        missing = set(keys) - set(project.keys())
        if missing:
            err("Missing keys %s for project %s"%(str(missing),project_name),7)
        for k in project:
            if k != 'alert_email' and not project[k]: #alert-email can be empty
                err("Key %s for project %s cannot be empty"%(k,project_name))
            elif k: project[k] = project[k].strip()

    for k in ['sr_url','sr_login','sr_pass']:
        if k not in sr:
            err("%s must be set for %s"%(k,_srkey),7)
        elif not sr[k]:
            err("%s is empty"%k,7)

    config[_srkey] = sr
    config['_hostname'] = hostname
    return config

def err(message,code):
    print >> sys.stderr, message
    sys.exit(code)

def argparser():
    from optparse import OptionParser
    parser = OptionParser('usage: %prog [options] project-name')
    
    parser.add_option("--bypass-sr", action='store_true',default=False,
                       help='Bypass the subject registry. Always sync subjects')
    parser.add_option("--nodel-diffexp", action='store_true',default=False,
                       help='Do not delete experiments that are of differing type')
    parser.add_option("--verbosity", action='store',type='int',default=1,metavar='1-4',
                       help='''Four verbosity levels
4 - log only changes
3 - log activity i.e syncing subject x
2 - log elements not synced (excluding files)
1 - log diagnostics (default)''')
    parser.add_option("--alert-email", action='store',type='string',
                      help='Email address to send log of skipped subjects/projects/experiments/etc')
    parser.add_option("--tech-email", action='store',type='string',
                      help='Email address to send diagnostic log')
    parser.add_option("--site-local", action='store',type='string',
                      help='Local XNAT url')
    parser.add_option("--site-remote", action='store',type='string',
                      help='Remote XNAT url')
    parser.add_option("--subject-create", action='store_true',default=True,
                      help='Automatic creation of non existent remote subjects. Defaults to True.')
    parser.add_option("--conf", action='store',default='/root/.xnat_sync.conf',
                      help='Config file location. Defaults to /root/.xnat_sync.conf')
    return parser

if __name__ == '__main__':
    parser=argparser()
    options,args=parser.parse_args()

    if len(args) == 0:
        parser.print_help()
        sys.exit(0)
    elif len(args) != 1:
        parser.print_usage()
        sys.exit(4)


    options=vars(options)
    config = parse_config(options)

    project = args[0]
    if project not in config:
        print >> sys.stderr, config
        err("Configuration for project %s was not given in /root/.xnat_sync.conf",7)

    sr_conf = config[_srkey]

    config = config[project] #only use config for specified project
    for k in config:
        config[k] = config[k].strip() #strip whitespace

    #let switches override config options
    for c in ["site_local","site_remote","tech_email","alert_email"]:
        if options[c]: config[c] = options[c]

    v=options['verbosity']
    if v<1 or v>4: err('--verbosity must be between 1-4',4)


    smtp_cred = {'user':0, 'pass':0, 'host':0, 'port':0}
    for k,c in [('user',"email_user"), ('pass',"email_pass"),
                ('host',"email_host"), ('port',"email_port")]:
        smtp_cred[k] = config[c]

    smtp_cred['from'] = 'Xnat sync: ' + socket.gethostname()

    logdir='/var/log/xnatsync'
    if os.path.exists(logdir):
        os.chmod(logdir,0700)
    else:
        os.mkdir(logdir,0700)

    l=logging.getLogger('xnatsync')
    _tech_log_name = _tech_log_name % project
    _pmanager_log_name = _pmanager_log_name % project

    #define four verbosity levels
    #v=4 - log only changes
    #v=3 - log activity i.e syncing subject x
    #v=2 - log elements not synced (excluding files)
    #v=1 - log diagnostics
    l.setLevel(1) #let everything through, handlers should filter based on level

    formatter=logging.Formatter(fmt='%(asctime)s %(message)s',datefmt='%m/%d/%Y %I:%M:%S %p')

    techlogh=handlers.RotatingFileHandler(_tech_log_name,backupCount=365)
    techlogh.setFormatter(formatter)
    techlogh.setLevel(v) #filter via verbosity here

    class lv2filter(logging.Filter):
        """This filter ensures that the project manager log only receives logging events based on elements not sync'd"""
        def filter(self, record):
            if record.levelno == 2: return True
            return False
    projectmh=handlers.RotatingFileHandler(_pmanager_log_name,backupCount=365)
    projectmh.setFormatter(formatter)
    lv2flt = lv2filter()
    projectmh.addFilter(lv2flt)
    projectmh.setLevel(2) #project manager should only ever log not sync'd elements

    streamh = logging.StreamHandler(sys.stdout)
    streamh.setFormatter(formatter)
    streamh.setLevel(v) #filter via verbosity here

    l.addHandler(techlogh)
    l.addHandler(projectmh)
    l.addHandler(streamh)

    def sendlog(eaddy,logfile,subject):
        '''send log file to target email addy'''
        try:
            with open(logfile) as lf:
                payload={eaddy: (lf.read(),None,subject + formatdate(localtime=True) )}
                email(smtp_cred,payload)
        except Exception, e:
            msg = 'Failed to send %s to %s' % (logfile,eaddy)
            l.log(4,msg)
            l.log(4,str(e))

    l.log(1,'Logging setup complete.')

    def launch():
        '''function to be called on repeat by daemonize()'''
        #do not append to old logs. reuse if empty, otherwise rotate

        if os.path.exists(_tech_log_name) and os.stat(_tech_log_name).st_size:
            techlogh.doRollover()
            l.log(1,'techlog rolled over')

        #send elements skipped summary to the project manager on each sync
        if os.path.exists(_pmanager_log_name) and os.stat(_pmanager_log_name).st_size:
            if config['alert_email']:
                sendlog(config['alert_email'],_pmanager_log_name,'Xnat Sync Summary: ')
            projectmh.doRollover()
            l.log(1,'project manager log rolled over')

        try:

            l.log(1,'Retrieving session for %s' % config['site_local'])
            sf1=getsessioncookiefile(config['site_local'],config['local_user'], config['local_pass'])
            l.log(1,'Retrieved.')

            l.log(1,'Retrieving session for %s' % config['site_remote'])
            sf2=getsessioncookiefile(config['site_remote'],config['remote_user'], config['remote_pass'])
            l.log(1,'Retrieved.')

            srcred=getpassfile(sr_conf['sr_login'],sr_conf['sr_pass'])

            syncProject(config['site_local'],config['site_remote'],project,sf1,sf2,
                        sr_conf['sr_url'],srcred.name,
                        options['bypass_sr'],options['nodel_diffexp'],options['subject_create'])

            sf1.close()
            sf2.close()
            srcred.close()

        except Exception, e:
            l.log(4,str(e))
            sendlog(config['tech_email'],_tech_log_name,'Xnat Sync Summary (tech): ')
            raise # crash script, but only after sending email to tech or logging failure

    def sigexit(signum, stack):

        #Ensure every handler logs signal handling events
        projectmh.removeFilter(lv2flt)
        projectmh.setLevel(1)
        techlogh.setLevel(1)
        streamh.setLevel(1)
        l.setLevel(1)
        sm=dict((k, v) for v, k in signal.__dict__.iteritems() if v.startswith('SIG') and not v.startswith('SIG_'))
        l.log(1,'Xnat sync program caught signal: %s. Syncing will now stop.' % sm[signum])

        #Reinstate filter because we don't log email failure notifications 
        projectmh.addFilter(lv2flt)

        #setup email
        if config['alert_email']:
            sendlog(config['alert_email'],_pmanager_log_name,'Xnat Sync Summary: ')
        projectmh.doRollover() #roll over to avoid sending same log next time script starts

        sendlog(config['tech_email'],_tech_log_name,'Xnat Sync Summary (tech): ')

        sys.exit(3)

    daemonize(launch,(),{},3600*12,{signal.SIGINT:sigexit,signal.SIGTERM:sigexit})
