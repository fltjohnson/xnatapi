#!/usr/bin/env python

from distutils.core import setup
import os
import shutil

# package_install_dir='__xadir'
# package_dir = os.path.join(package_install_dir,'xnatapi')

os.chdir(os.path.abspath(os.path.dirname(__file__)))
# if not os.path.exists(package_install_dir):
#     os.mkdir(package_install_dir)
#     os.mkdir(package_dir)

# files=os.listdir('.')
# files.remove('setup.py')
# files.remove('.git')
# files.remove(package_install_dir)
# if os.path.exists('build'): files.remove('build')

# for f in files:
#     if os.path.isdir(f):
#         shutil.copytree(f,os.path.join(package_dir,f))
#     else:
#         shutil.copy2(f,package_dir)

setup(name='xnatapi',
      version='1.0',
      description='xnat interface + sync',
      author='Fletcher Johnson',
      author_email='fjohnson@research.baycrest.org',
      license='Simplified BSD License',
      package_dir={'xnatapi':os.path.abspath(os.path.dirname(__file__))},
      packages=['xnatapi','xnatapi.xnatsync'],
      package_data={'xnatapi':['license']},
      scripts=['xnatsync/xnatsync.py'],
      data_files=[('/etc/init.d', ['xnatsync/etc/xnatsync_init.py'])]
     )

#shutil.rmtree(package_install_dir)
