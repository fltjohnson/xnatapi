import re
import logging as l

'''This module holds functions for stripping an xnat xml document of unsyncable items.
Once stripped the document can be sent to xnat provided that the 
project/subject/experiment/scans have already been created. This module is for syncing
metadata'''

#TODO Log all of this information to a seperate file.
import logging
l = logging.getLogger("xnatsync")

namespaces={'xnat':"http://nrg.wustl.edu/xnat",
            'xsi':"http://www.w3.org/2001/XMLSchema-instance"}

def remove_blacklisted_resource(doc):
    blist=set(['xnat:resourceSeries', 'xnat:resource', 'xnat:imageResource', 
               'xnat:imageResourceSeries', 'xnat:resourceCatalog'])
    
    rez=doc.xpath('//@xsi:type/..',namespaces=namespaces)
    type_attr='{'+namespaces['xsi']+'}'+'type'
    blacklisted_rez=filter(lambda x : x.attrib[type_attr] in blist, rez)

    for r in blacklisted_rez: 
        rp = r.getparent()
        ra = rp

        #keep climing up the tree until a relevant identifer is found. projects/subjects
        #are guaranteed to have at least an ID

        while not 'ID' in ra.attrib and not 'label' in ra.attrib: ra = ra.getparent()

        if 'label' in ra.attrib:
            if 'label' in r.attrib:
                l.log(0, 'Skipping %s label:%s in %s label:%s' % (r.attrib[type_attr],r.attrib['label'],
                                                              ra.tag, ra.attrib['label']))
            elif 'ID' in r.attrib:
                l.log(0, 'Skipping %s ID:%s in %s label:%s' % (r.attrib[type_attr],r.attrib['ID'],
                                                              ra.tag, ra.attrib['label']))
            else:
                l.log(0, 'Skipping %s in %s label:%s' % (r.attrib[type_attr],ra.tag, ra.attrib['label']))
        else:
            if 'label' in r.attrib:
                l.log(0, 'Skipping %s label:%s in %s ID:%s' % (r.attrib[type_attr],r.attrib['label'],
                                                           ra.tag, ra.attrib['ID']))
            elif 'ID' in r.attrib:
                l.log(0, 'Skipping %s ID:%s in %s ID:%s' % (r.attrib[type_attr],r.attrib['ID'],
                                                           ra.tag, ra.attrib['ID']))
            else:
               l.log(0, 'Skipping %s in %s ID:%s' % (r.attrib[type_attr],ra.tag, ra.attrib['ID']))
        rp.remove(r)

    rez=filter(lambda x : x.attrib[type_attr] == 'xnat:dicomSeries', rez)
    for r in rez:
        if 'cachePath' in r.attrib:
            cpath = r.attrib['cachePath']
            del r.attrib['cachePath']
            if 'label' in r.attrib:
                l.log(0, 'Skipping @cachePath=%s in %s label:%s'%(cpath, r.tag,r.attrib['label']))
            else:
                l.log(0, 'Skipping @cachePath=%s in %s ID:%s'%(cpath, r.tag,r.attrib['ID']))
        iset=r.xpath('./xnat:imageSet',namespaces=namespaces)
        if iset: 
            iset[0].getparent().remove(iset[0])
            if 'label' in r.attrib:
                l.log(0, 'Skipping xnat:imageSet in %s label:%s'%(r.tag,r.attrib['label']))
            else:
                l.log(0, 'Skipping xnat:imageSet in %s ID:%s'%(r.tag,r.attrib['ID']))

def remove_hidden_fields(doc):
    
    for c in doc.xpath('//comment()'):
        if re.match("hidden_fields\[.*?\]",c.text):
            #c.getparent().remove(c)
            #an lxml bug prevents me from doing the above. try out this code to see what I mean.
            # from lxml import etree
            # doc=etree.fromstring('''<one>
            #  <two>40.0<!--love--> a</two>
            # </one>''')
            # for c in doc.xpath('//comment()'):
            #   c.getparent().remove(c)
            # print etree.tostring(doc,pretty_print=True)
            #it should return ...<two>40.0</two> instead of <two>40.0 a</two>
            l.log(0, 'Skipping comment ' + c.text)
            c.text='' #blank the comment instead

def remove_investigator_IDS(root):

    #investigator elements in a project
    pi=root.xpath("./xnat:PI",namespaces=namespaces)
    investigators=root.xpath("./xnat:investigators/xnat:investigator",namespaces=namespaces)

    #in an experiment
    investigator=root.xpath("./xnat:investigator",namespaces=namespaces)
    pi=pi+investigators+investigator
    while pi:
        inv = pi.pop()
        if 'ID' in inv.attrib: 
            id=inv.attrib['ID']
            del inv.attrib['ID']
            l.log(0, 'Deleted ID:%s of %s' %(id, inv.tag))

def remove_shares(root):
    for s in root.xpath("//xnat:sharing",namespaces=namespaces):
        sp=s.getparent()
        sp.remove(s)
        if 'label' in sp.attrib:
            spl = sp.attrib['label']
        elif sp.tag=='{'+namespaces['xnat']+'}scan':
            spl = 'ID:' + sp.attrib['ID']
        else: spl = 'NA'
        l.log(0, 'Skipping xnat:sharing in %s %s' % (sp.tag, spl))

def fixup_project_xml(root):
    remove_blacklisted_resource(root)
    remove_hidden_fields(root)
    remove_investigator_IDS(root)
    sp = root.xpath("./xnat:studyProtocol",namespaces=namespaces)
    if sp:
        sp = sp[0]
        if 'ID' in sp.attrib: 
            id=sp.attrib['ID']
            del sp.attrib['ID']
            l.log(0, 'Deleted ID:%s of %s' %(id, sp.tag) )
    return root

def fixup_scan_xml(root,projid,expid):

    if 'project' in root.attrib: root.attrib['project']=projid
    isi=root.xpath("./xnat:image_session_ID",namespaces=namespaces)[0]
    isi.text=expid

def fixup_subject_xml(root,subid,projid,expid_map,expskip_list=None):
    if expskip_list: # skip syncing certain experiments
        type_attr='{'+namespaces['xsi']+'}'+'type'
        for e in root.xpath("./xnat:experiments/xnat:experiment",namespaces=namespaces):
            if e.attrib['label'] in expskip_list:
                l.log(0, 'Skipping experiment metadata sync %s label: %s' % (e.attrib[type_attr],e.attrib['label']))
                e.getparent().remove(e)

    remove_investigator_IDS(root)
    remove_blacklisted_resource(root)
    remove_shares(root)
    remove_hidden_fields(root)
    root.attrib['ID']=subid
    root.attrib['project']=projid
    
    for e in root.xpath("./xnat:experiments/xnat:experiment",namespaces=namespaces):

        #begin items found in xnat:subjectAssessorData
        remove_investigator_IDS(e)
        sid=e.xpath("./xnat:subject_ID",namespaces=namespaces)[0]
        sid.text=subid
        e.attrib['project']=projid
        e.attrib['ID']=expid_map[e.attrib['label']]
        #end items found in xnat:subjectAssessorData

        #begin items found in xnat:imageSessionData
        prearchivePath=e.xpath("./xnat:prearchivePath",namespaces=namespaces)
        if prearchivePath:
            l.log(0,  'Skipping xnat:prearchivePath in %s label:%s' % (e.tag, e.attrib['label']))
            prearchivePath[0].getparent().remove(prearchivePath[0])

        regions=e.xpath("./xnat:regions/xnat:region",namespaces=namespaces)
        for r in regions:
            r.attrib['session_id']=expid_map[e.attrib['label']]

        recon=e.xpath("./xnat:reconstructions",namespaces=namespaces)
        if recon:
            l.log(0,  'Skipping xnat:reconstructions in %s %s' % (e.tag, e.attrib['label']))
            recon[0].getparent().remove(recon[0])

        assessor=e.xpath("./xnat:assessors",namespaces=namespaces)
        if assessor:
            l.log(0,  'Skipping xnat:assessors in %s %s' % (e.tag, e.attrib['label']))
            assessor[0].getparent().remove(assessor[0])

        scans=root.xpath("./xnat:experiments/xnat:experiment/xnat:scans/xnat:scan",
                         namespaces=namespaces)
        for s in scans:
            fixup_scan_xml(s,projid,e.attrib['ID'])
        
    return root

