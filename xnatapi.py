import os
import re
import xml
import time
import tempfile
import shutil
from lxml import etree
from sysutil import eproc,c,scriptFailed
from xml.etree import ElementTree
from tempfile import NamedTemporaryFile
from xnatxmlformat import fixup_subject_xml,fixup_project_xml
from zipfile import ZipFile

'''This module contains functions I use for interacting with XNAT.'''

SYNC_DRYRUN=0
XML_UPLOAD_TEMP_FILE=NamedTemporaryFile('wb')

import logging
#use logger defined in xnatsync.py
#if not using xnatsync, default logger produces no output
l = logging.getLogger("xnatsync")

try:
    from ast import literal_eval
except ImportError:
    #for python 2.5 compat
    #source: http://stackoverflow.com/questions/5642987/converting-tuple-to-dictionary
    from compiler import parse
    from compiler.ast import *

    def literal_eval(node_or_string):
        """
        Safely evaluate an expression node or a string containing a Python
        expression.  The string or node provided may only consist of the  
        following Python literal structures: strings, numbers, tuples, 
        lists, dicts, booleans, and None.
        """
        _safe_names = {'None': None, 'True': True, 'False': False}
        if isinstance(node_or_string, basestring):
            node_or_string = parse(node_or_string, mode='eval')
        if isinstance(node_or_string, Expression):
            node_or_string = node_or_string.node
        def _convert(node):
            if isinstance(node, Const) and isinstance(node.value,
                    (basestring, int, float, long, complex)):
                 return node.value
            elif isinstance(node, Tuple):
                return tuple(map(_convert, node.nodes))
            elif isinstance(node, List):
                return list(map(_convert, node.nodes))
            elif isinstance(node, Dict):
                return dict((_convert(k), _convert(v)) for k, v
                            in node.items)
            elif isinstance(node, Name):
                if node.name in _safe_names:
                    return _safe_names[node.name]
            elif isinstance(node, UnarySub):
                return -_convert(node.expr)
            raise ValueError('malformed string')
        return _convert(node_or_string)

class xnatApiException(Exception): pass

def syncProject(url, rurl, name, s1, s2, srurl, srcred,
                bypass_sr=False, delete_diff_exp=True, auto_create_subjects=True,
                auto_create_projects=False):

    '''Sync a project
    url: src url
    rurl: remote url
    name: name of the project
    s1: sessionid for url
    s2: sessionid for rurl
    srurl: subject registry url
    srcred: a curl config file containing the user/pass for the sr. see getpassfile()
    bypass_sr: skip the sr checks entirely
    delete_diff_exp: delete remote experiments that differ from their local counterparts
    auto_create_subjects: automatically create remote subjects if they don't exist.
     '''

    # Return codes from the cgi reporter and the responses this function takes are ...
    # On -2: Project id isn't found - project skipped.
    # On -1: CGI Reporter error - script exits.
    # On 0: Subject is not found in the project - subject skipped.
    # On 1: Subject found but not verified - subject skipped.
    # On 2: Subject is found and verified - subject sync'd.
    #srurl='https://10.3.13.6:8443/spred/cgi-bin/sr-query?p=%s&s=%s'
    #srurl='https://spred.braincode.ca/spred/cgi-bin/sr-query?p=%s&s=%s'


    if not bypass_sr:
        srurl=surljoin(srurl,'cgi-bin/sr-query?p=%s&s=%s')
        cgiurl=srurl%(name,'na')
        l.log(4,'Calling SR url %s'%cgiurl)

        so,er=qcurlcall(cgiurl,passFile=srcred) #session can be anything
        match=re.match('<ret>(-2|-1|0|1|2)</ret>',so.strip())
        if not match:
            raise xnatApiException('CGI reporter returned unexpected value %s' %(so.strip()))
        retcode=int(match.group(1))

        if retcode == -1:
            raise xnatApiException("Communications with the Subject Registry failed. SR returned -1")
        elif retcode==-2:
            l.log(2, "Project was not found in the Subject Registry. Not syncing.")
            return 2

    if not name in getJSON(surljoin(url,'data','archive','projects'),'ID',session=s1):
        l.log(2, 'Project %s must exist locally. Not syncing.' % name)
        return 1

    ppath=surljoin('data','archive','projects',name)

    if not name in getJSON(surljoin(rurl,'data','archive','projects'),'ID',session=s2):
        if not auto_create_projects:
            l.log(2, 'Project %s must exist remotely. Not syncing.' % name)
            return 3
        qcurlcall(surljoin(rurl,ppath), method='PUT', session=s2)
        l.log(1, 'Created remote project %s' % name)

    l.log(3, 'Syncing project ' + name)
    
    #sync project level metadata
    projxml=getXMLTree(surljoin(url,ppath),s1)
    fixup_project_xml(projxml)
    submitXMLTree(projxml,surljoin(rurl,ppath),s2)
    l.log(1, 'Sync\'d project metadata')

    pid = projxml.attrib['ID']
    s1sub=getJSON(surljoin(url,ppath,'subjects'),'label',session=s1)
    s2sub=getJSON(surljoin(rurl,ppath,'subjects'),'label',session=s2)

    syncFiles(url,rurl,ppath,s1,s2)
    for s in s1sub:

        if bypass_sr:
            retcode=2 #override sr check
        else:
            cgiurl=srurl%(name,s)
            l.log(4,'Calling SR url %s'%cgiurl)

            so,er=qcurlcall(cgiurl,passFile=srcred) #session can be anything
            match=re.match('<ret>(-2|-1|0|1|2)</ret>',so.strip())
            if not match:
                raise xnatApiException('CGI reporter returned unexpected value %s' %(so.strip()))
            retcode=int(match.group(1))

            if not retcode in [0,1,2,-2]:
                raise xnatApiException('CGI reporter returned value %s for %s' %(str(retcode),cgiurl))
        if retcode==2:
                if not s in s2sub: #if the subject doesn't already exist, create it
                    if not auto_create_subjects:
                        l.log(2, 'Skipping subject %s. --subject-create=False.' % s)
                        continue
                    l.log(4, 'Creating subject ' + s)
                    putSubject(surljoin(rurl,ppath), s, s2)
                syncSubject(url,rurl,ppath,s,pid,s1,s2,delete_diff_exp)
        elif retcode==1:
            l.log(2, 'Skipping subject %s. Subject not verified.' % s)
        elif retcode==0:
            l.log(2, 'Skipping subject %s. Subject not found.' % s)
        elif retcode==-2:
            l.log(2, "Project was not found in the Subject Registry. Not syncing.")
            return 2

    return 0


def getSubjectFixupInfo(subjecturl,session):

    """This function returns information necessary to call 'fixup_subject_xml()'
     It should be used if you are calling that function indepedently of syncSubject()
     'subjecturl' is a complete url, i.e https://...."""

    projecturl=subjecturl.rsplit('/',2)[0]
    projtree=getXMLTree(projecturl,session)
    pid = projtree.attrib['ID']
    subjectree=getXMLTree(subjecturl,session)
    sid=subjectree.attrib['ID']
    exp_labelandid_map=dict(getJSON(surljoin(subjecturl,'experiments'),'label','ID',session=session))
    return sid,pid,exp_labelandid_map

def syncSubject(url,rurl,ppath,s,pid,s1,s2,del_diffexp=True):
    '''Sync a subject
    url: src url
    rurl: remote url
    ppath: data/archive/projects/project_name
    s: subject's label
    pid: the projects ID attribute
    s1: sessionid for url
    s2: sessionid for rurl
    del_diffexp: delete experiments on the remote side that differ in xsiType.
    replace with an experiment of the same type as the local experiment
    '''

    l.log(3, 'Syncing subject ' + s)
    spath=surljoin(ppath,'subjects',s)
    syncFiles(url,rurl,spath,s1,s2)
    exp1=getJSON(surljoin(url,spath,'experiments'),'label','xsiType',session=s1)
    exp2=getJSON(surljoin(rurl,spath,'experiments'),'label','xsiType',session=s2)
    exp1label = set(map(lambda t: t[0],exp1)) 
    exp2label = set(map(lambda t: t[0],exp2)) 
    expskip_list=[]

    for e in exp2label-exp1label:
        l.log(2, 'Skipping remote experiment experiment ' + e)

    for e in exp1-exp2: 
        eurl=surljoin(rurl,spath,'experiments',e[0])
        if e[0] in exp2label: #experiment exists, just diff type so delete still
            if not del_diffexp:
                l.log(2, 'Skipping sync of experiment of differing type ' + e[0])
                expskip_list.append(e[0])
                continue #skip deleting differing exp and don't sync
            l.log(4, 'Deleting existing experiment (different type): ' + eurl)
            if not SYNC_DRYRUN: deleteItem(eurl,s2)
            else: l.log(3, 'SYNC_DRYRUN: experiment not really deleted - will have same type on creation')
        l.log(4, 'Creating experiment: ' + eurl)
        if not SYNC_DRYRUN:putExperiment(surljoin(rurl,spath),e[0],e[1],s2)
                
        syncExperiment(url,rurl,spath,e[0],s1,s2)
    for e in exp1 & exp2:
        syncExperiment(url,rurl,spath,e[0],s1,s2)

    #submit subject,experiment and scan metadata
    subjects=getJSON(surljoin(rurl,ppath,'subjects'),'label','ID',session=s2)
    for label,subid in subjects:
        if label == s: sid = subid

    exp2labelandid=getJSON(surljoin(rurl,spath,'experiments'),'label','ID',session=s2)
    exp2labeltoid_map=dict(exp2labelandid)

    subxml=getXMLTree(surljoin(url,spath),s1)
    fixup_subject_xml(subxml,sid,pid,exp2labeltoid_map,expskip_list)
    submitXMLTree(subxml,surljoin(rurl,spath),s2)
    l.log(1, 'Sync\'d subject/experiment/scan metadata for ' + s)

def getXMLTree(path,session):
    so,er=qcurlcall(surljoin(path,'?format=xml'),session=session)
    return etree.fromstring(so)

def submitXMLTree(tree,path,session):
    xmltree=etree.tostring(tree,pretty_print=True)
    qcurlcall(path,session=session,stdin=xmltree)

def syncExperiment(url,rurl,spath,ename,s1,s2):
    epath=surljoin(spath,'experiments',ename)
    syncFiles(url,rurl,epath,s1,s2)

    typemap=dict(getJSON(surljoin(url,epath,'scans'),'ID','xsiType',session=s1))
    scans1 = set(typemap.keys())

    if SYNC_DRYRUN:
        #experiment was not created? (dryrun)
        if not ename in getJSON(surljoin(rurl,spath,'experiments'),'label',session=s2):
            scans2=set() #pretend experiment was created
        else:scans2=getJSON(surljoin(rurl,epath,'scans'),'ID',session=s2)
    else:scans2=getJSON(surljoin(rurl,epath,'scans'),'ID',session=s2)

    for s in scans1 - scans2:
        l.log(4, 'Creating scan: ' + surljoin(rurl,epath,'scans',s))
        if not SYNC_DRYRUN:putScan(surljoin(rurl,epath),s,s2, xsiType=typemap[s])
        syncFiles(url,rurl,surljoin(epath,'scans',s),s1,s2)
    for s in scans1 & scans2:   
        syncFiles(url,rurl,surljoin(epath,'scans',s),s1,s2)
    for s in scans2 - scans2:
        l.log(2, 'Skipping remote scan ' + s)

def syncResource(res_local, res_remote, files, s1,s2):
    '''Sync a resource

    res_local: The url of the resource on the local target
    i.e https://localhost:8443/spred/data/archive/projects/training/subjects/12Chn_DailyQC/experiments/1/resources/txt

    res_remote: The url of the resource on the remote target
    i.e https://spred.braincode.ca/spred/data/archive/projects/training/subjects/12Chn_DailyQC/experiments/1/resources/txt

    files: A list files to sync

    s1,s2: sessions for the local and remote machines respectively'''

    tdir = tempfile.mkdtemp()
    cwd = os.getcwd()
    os.chdir(tdir)

    try:
        #Upload snapshots specially
        split = res_local.split('/')
        if split[-4] == 'scans' and split[-2] == 'resources' and split[-1] == 'SNAPSHOTS':
            if len(files) > 2:
                l.log(1, '> 2 SNAPSHOT files exist, guessing thumbnail')
            l.log(1, 'Uploading files %s: %s' % (res_remote,files))

            for f in files:
                qcurlcall(surljoin(res_local,'files',f),'GET',s1,customarg=['-o',f])
                if f.endswith('_t.gif'):
                    qcurlcall(surljoin(res_remote,'files',f+'?content=THUMBNAIL&inbody=true'),method='PUT',
                              session=s2,customarg=['-T',f])
                else:
                    qcurlcall(surljoin(res_remote,'files',f+'?content=ORIGINAL&inbody=true'),method='PUT',
                              session=s2,customarg=['-T',f])
            return

        #Skip doing a zipped upload if the number of files being uploaded is less than 10
        if len(files) < 10:
            for f in files:
                qcurlcall(surljoin(res_local,'files',f),'GET',s1,customarg=['-o',f])
                qcurlcall(res_remote,session=s2,files=[f])
            return

        qcurlcall(surljoin(res_local,'files?format=zip'),'GET',s1,customarg=['-o','zip.zip'])

        z = ZipFile('zip.zip')
        for f in z.namelist(): z.extract(f)
        z.close()
        os.unlink('zip.zip')

        for root, dirs, filelist in os.walk('.'):
            for f in filelist: os.rename(os.path.join(root,f), os.path.join(tdir,f))

        z = ZipFile('zipupload.zip','w')
        for f in files: z.write(f)
        z.close()

        l.log(1, 'Uploading files (zip) %s: %s' % (res_remote,files))
        qcurlcall(surljoin(res_remote,'files/zipupload.zip?extract=true'),
                  method='PUT',session=s2,customarg=['-F','zipupload.zip=@zipupload.zip'])
    finally:
        os.chdir(cwd)
        shutil.rmtree(tdir)

def syncFiles(url,rurl,item,s1,s2):
    '''copy files from one project/subject/experiment/scan to another

    Examples of arguments

    item: a path to a particular project/subject/experiment/scan
    for instance...
    /data/archive/projects/p1 or
    /data/archive/projects/p1/subjects/s1 or
    /data/archive/projects/p1/subjects/s1/experiments/e1

    url: The local url prefix to be combined with the item.
    For instance: https:/spred.braincode.ca/spred/

    rurl: same as url but a remote url.

    s1,s2: session keys for url and rurl respectively.

    an invocation of syncFiles(https:/spred.braincode.ca/spred/,
                               https:/spreddev.braincode.ca/spred/,
                               /data/archive/projects/p1/subjects/s1,
                               key1,key2)
    would copy any resources and files in https:/spred.braincode.ca/spred/data/archive/projects/p1/subjects/s1
    to https:/spreddev.braincode.ca/spred/data/archive/projects/p1/subjects/s1. This operation is not recursive
    and would not sync s1's experiments in this example.
    '''

    l.log(3, 'Syncing files: ' + item)

    #Get a list of files that exist only on url and not rurl. This includes files that differ in size.
    fset1=getJSON(surljoin(url,item,'files'),'Name','collection','Size',session=s1)
    fset2=getJSON(surljoin(rurl,item,'files'),'Name','collection','Size',session=s2)
    reqset = fset1 - fset2

    #Don't sync empty files. Curl will not download empty files.
    for f in set(reqset):
        if f[2] == '0': reqset.remove(f)

    l.log(1,'Diff %s' % reqset)

    #create a mapping of resource to files needed to be uploaded
    resource_to_files = {}
    for t in reqset:
        if t[1] in resource_to_files: resource_to_files[t[1]].append(t[0])
        else:resource_to_files[t[1]] = [t[0]]

    #sync resources
    if '' in resource_to_files:
        #This is a special case where a resource with no name exists. It is impossible
        #to create this empty resource on the remote side if it doesn't already exist AND
        #other resources exist as well. It can only be created when no existing resources
        #are present

        #Get the id of the null resource, we need this to properly address it
        rset1=getJSON(surljoin(url,item,'resources'),'label','xnat_abstractresource_id',session=s1)
        rset2=getJSON(surljoin(rurl,item,'resources'),'label','xnat_abstractresource_id',session=s2)

        for t in rset1:
            if t[0] == '': nid1 = t[1]
        nid2 = None
        for t in rset2:
            if t[0] == '': nid2 = t[1]

        #if nid2 is None, then we need to create the null resource on the remote side
        if nid2 == None:
            if not rset2:
                #no other resource exist. we can create the null resource by uploading directly to url/item/files
                l.log(4, 'Creating null resource: ' + surljoin(rurl,item,'files'))
                syncResource(surljoin(url,item,'resources',nid1),surljoin(rurl,item),resource_to_files[''],s1,s2)
            else:
                #impossible to create. sync to _nolabel
                l.log(4, 'Cannot create null resource: using %s instead' % surljoin(rurl,item,'resources','_nolabel'))
                if '_nolabel' not in map(lambda x: x[0], rset2):
                    putResource(surljoin(rurl,item),'_nolabel',s2)
                syncResource(surljoin(url,item,'resources',nid1),surljoin(rurl,item,'resources','_nolabel'),
                             resource_to_files[''],s1,s2)
        else:
            syncResource(surljoin(url,item,'resources',nid1),surljoin(rurl,item,'resources',nid2),
                         resource_to_files[''],s1,s2)
        del resource_to_files['']

    #Get a list of the resources that exist remotely so that we can create the ones that dont.
    rurl_resources = getJSON(surljoin(rurl,item,'resources'),'label',session=s2)
    for r in resource_to_files:
        if r not in rurl_resources:
            l.log(4, 'Creating resource: %s' % surljoin(rurl,item,'resources',r))
            putResource(surljoin(rurl,item),r,s2)
        syncResource(surljoin(url,item,'resources',r),surljoin(rurl,item,'resources',r),
                         resource_to_files[r],s1,s2)


def archive(zipfpath,host,project,session):
    prearcToArc(host,prearc(zipfpath,host,project,session),session)
    
def prearc(zipfpath,host,project,session):
    '''store session in prearc

    zipfpath - path to zip file
    host - i.e https://localhost:8443/spred/
    project - project label
    '''

    customarg=['--data-binary', '@%s'%zipfpath, '-H', 'Content-Type: application/zip']
    url=surljoin(host,"/data/services/import?project=%s&prearchive=true&inbody=true"%project)
    sout,ret=qcurlcall(url,"POST",session,customarg=customarg)
    if re.match('/data/prearchive/projects/%s/\d+_\d+/.+'%project,sout):
        return sout.strip()[len('/data'):]
    else:raise xnatApiException('Prearc store command returned bad stuff: %s' % sout)
    
def prearcToArc(host,prearc_url,session):
    '''move prearc to arc

    host - i.e https://localhost:8443/spred/
    prearc_url - i.e regex /data/prearchive/projects/IGL_BUILD/\d+_\d+/.+

    '''

    project=prearc_url.split('/')[3]
    url=surljoin(host,"data/services/archive?src=%s&overwrite=delete&dest=/archive/projects/%s/"%(prearc_url,project))
    while True:

        so,se,r=curlcall(url,method='POST',session=session)
        if r==0:return
        if 'Operation Failed: Session processing in progress:xnat_tools/AutoRun.xml' in so:
            time.sleep(60)
        else:
            #be careful here. remember that so,se are the output from the _second_ time
            #curl is called if the first time resulted in an error. so,se may contain
            #successful output if no error resulted on the second call.
            raise xnatApiException(so)

    
def getLabelLocalURI(url,xnatURI,session1,isScan):

    """Generate a mapping between a particular resource and its location locally

    For instance:

    <xnat:file label="SNAPSHOTS"
    URI="/usr/xnat/spred/archive/IGT_GLIOMA/arc001/00001/SCANS/2/SNAPSHOTS/SNAPSHOTS_catalog.xml"
    format="GIF" content="SNAPSHOTS" xsi:type="xnat:resourceCatalog"/>

    Here the SNAPSHOTS resource is mapped to the URI attribute. This function is needed so that the
    files for a particular resource can be located on the hard disk before being uploaded"""

    t=surljoin(url,xnatURI,'?format=xml')
    parsethis=qcurlcall(t,session=session1)[0]

    try: tree = ElementTree.fromstring(parsethis)
    except xml.parsers.expat.ExpatError as e:
        raise xml.parsers.expat.ExpatError(parsethis + '\n' +e.message)
    ename='{http://nrg.wustl.edu/xnat}resources' if not isScan else '{http://nrg.wustl.edu/xnat}file'

    labelmap = {}

    if not isScan: 
        tree = tree.find(ename) #find first resources element only
        if not tree: return labelmap#no resources
        ename='{http://nrg.wustl.edu/xnat}resource'

    for child in tree.getiterator(ename):
        if 'label' in child.attrib: key=child.attrib['label']
        else:key=''
        labelmap[key]=child.attrib['URI'].rpartition('/')[0]

    return labelmap    

def getpassfile(user,passw):
    '''Use this function to return a curl configuration file with the user/pass
    This acts as a secure way to pass user credentials to curl'''
    passfile = NamedTemporaryFile('wb')
    passfile.write('user=%s:%s\n' % (user, passw))
    passfile.flush()
    os.fsync(passfile.fileno())
    return passfile

def getsessioncookiefile(url,user,passw):
    '''Get an xnat session and store it using curl's cookiejar format.
    Return the cookiejar which can be used to set the JSESSION header
    This provides a secure way to pass the session id to curl'''

    pf = getpassfile(user,passw)
    so,er=qcurlcall(surljoin(url,'data','JSESSION'),passFile=pf.name,method='POST',customarg=['-c','-'])
    pf.close()
    cookiejar=NamedTemporaryFile('wb')
    cookiejar.write(so)
    cookiejar.flush()
    os.fsync(cookiejar.fileno())
    return cookiejar

def curlcall(url, method='GET', stdout=None, stderr=None, 
             stdin=None, session=None,files=None, passFile=None, customarg=[]):
    '''Make a call to curl for "url"'''
    argz = [ "curl", '--silent','--fail','-k','-m','3600'] + customarg

    if session and hasattr(session,'fileno'): 
        argz += ['--cookie', session.name]
    elif session:argz += ['--cookie', 'JSESSIONID='+session]
    elif passFile: argz += ['-K',passFile] #use a curl config file for the password

    if stdin: 
        if files: raise xnatApiException("Mixing of -T and -F not allowed")
        if hasattr(stdin,'fileno'):
            argz += ['-T',stdin.name,url]
        else:
            argz += ['-T',XML_UPLOAD_TEMP_FILE.name,url]
        #curl uploads stdin chunked and xnat doesn't support chunked transfers
        #therefore we need to use a file
            XML_UPLOAD_TEMP_FILE.seek(0)
            XML_UPLOAD_TEMP_FILE.truncate()
            XML_UPLOAD_TEMP_FILE.write(stdin) #stdin is a string
            XML_UPLOAD_TEMP_FILE.flush()
            os.fsync(XML_UPLOAD_TEMP_FILE.fileno())

    elif files: #doing an upload...
        for f in files: 
            if not os.path.exists(f): 
                raise xnatApiException('Cwd: %s File: %s DNE %s' % (os.getcwd(),f,`os.listdir('.')`))
            argz.append('-F')
            argz.append('%s=@%s'%(f,f))
        urlf = str(urljoin(url,'files'))
        argz.append(urlf)
        #l.info(' '.join(argz))
    else: argz += ['-X',method, url]
   
    so,se,r=c(argz,largs=True,ignore_rc=True,stdout=stdout, stderr=stderr,stdin=stdin)
    if r != 0:
        argz[2] = '--show-error'

        #remove -o and its argument. otherwise the error message will be redirected to a
        #file and it will not be captured
        try:
            i = argz.index('-o')
            argz = argz[0:i] + argz[i+2:]
        except ValueError: pass
        return c(argz,largs=True,ignore_rc=True,stdout=stdout, stderr=stderr,stdin=stdin)[:2]+(r,)
    return so,se,r

def qcurlcall(target,method='GET',session=None,files=None,stdin=None,passFile=None,customarg=[]):
    
    so,er,ret = curlcall(target,method,session=session,
                         files=files,stdin=stdin,passFile=passFile,customarg=customarg)

    if ret != 0: 
        message=os.linesep + er + os.linesep + so + os.linesep + "URL: " + target + os.linesep
        raise xnatApiException("curl returned %d %s" % (ret,message))  
    return so,er

def getItemURL(url,item):
    '''Find all instances of "item" at "url". 
    Valid items are subjects/experiments/reconstructions'''

    itemdict = {'subjects': '"/data/subjects/(.+?)"',
                'experiments': '<xnat:experiment ID="(.+?)"',
                'reconstructions':'<xnat:reconstructedImage ID="(.+?)">'}
    
    stdout,stderr,retcode = curlcall(url)
    #print stdout
    return re.findall(itemdict[item], stdout)

def surljoin(base,*rest,**kwargs):
    return str(urljoin(base,*rest,**kwargs))

def urljoin(base,*rest,**kwargs):
    '''Join two url segments together optionally separated by delimiter.
    So if base was http://google.com and rest was news then the result
    would be http://google.com/news. This function returns a 'urljoined'
    object that allows chaining so you can say something like:

    urljoin("http://google.com","news").
    urljoin("forensics").
    urljoin("crypt","necessary")
    -> http://google.com/news/forensics/crypt/necessary'''

    if len(kwargs) > 1 or (len(kwargs) == 1 and not 'delimiter' in kwargs):
        raise(TypeError("Expecting keyword arg 'delimiter' only."))
    
    if kwargs:
        delimiter = kwargs['delimiter']
    else: delimiter = '/'
    
    class urljoined:
        def __init__(self,string): 
            self.string=string
            self.delimiter = delimiter

        def urljoin(self,rest,*more):
            return urljoin(self.string, *( (rest,) + more),
                           **{'delimiter':self.delimiter})

        def __add__(self,other): return self.string+other
        def __radd__(self,other): return other + self.string
        def __str__(self): return self.string
        def __repr__(self): return self.string
        def __getitem__(self,n): return self.string[n]

    if base[-1] == delimiter: base = base[:-1]
    if rest[0][0] == delimiter: rest=(rest[0][1:],)+rest[1:]
    return urljoined(delimiter.join( (base,) +rest ))

def deleteItem(url,session=None):
    if url[-1]=='/': url=url[:-1]
    qcurlcall(url+'?removeFiles=true',method='DELETE',session=session)
        
def putProject(base,pname,session=None):
    target = urljoin(base,'data','archive','projects',pname)
    qcurlcall(str(target),'PUT',session)
    return `target`

def putSubject(proj,subj,session=None):
    '''Create a new subject
    where proj = https://.../projects/pname
    subj = subject's name '''
    target = urljoin(proj,'subjects',subj)
    qcurlcall(str(target),'PUT',session)
    return `target`

def putExperiment(subj, exp, etype='xnat:MRSession' ,session=None, efields=[]):
    '''Create a new experiment.
    subj is the full url path to the subject
    exp the exp name
    etype is xnat:MRSession, ...
    efields are optional fields for setting experiment data such as the scanner
    operator or type'''

    target = surljoin(subj,'experiments',exp+'?xsiType='+etype)
    for f in efields: target += '&'+f+'='+efields[f]
    qcurlcall(target,'PUT',session)
    return surljoin(subj,'experiments',exp)

def putScan(exp, scan, session=None, stype=None, xsiType='xnat:mrScanData', quality='usable'):

    '''Create a new scan
    exp is the full url path to the experiment
    scan is the scan name
    stype is a type such as dicom or nifti
    quality is useable,questionable,...
    xsiType is xnat:mrScanData, xnat:eegScanData...
    '''

    target = surljoin(exp,'scans',scan)
    if stype: target += '?type='+stype
    if xsiType:
        if stype: target += '&xsiType='+xsiType
        else: target += '?xsiType='+xsiType
    if quality:
        if stype or xsiType: target += '&quality='+quality
        else: target += '?quality='+quality

    qcurlcall(target,'PUT',session)
    return surljoin(exp,'scans',scan)

def putReconstruction(exp,recon,session=None):
    target = urljoin(exp,'reconstructions',recon)
    qcurlcall(str(target),'PUT',session)
    return `target`

def putResource(url,rezname,session=None):
    target = urljoin(url,'resources',rezname)
    qcurlcall(str(target),'PUT',session)
    return `target`

def putAssessor(exp,asor,session=None):
    target = urljoin(exp,'assessors',asor)
    qcurlcall(str(target),'PUT',session)
    return `target`

def getProjects(base,session=None):
    target = urljoin(base,'data','archive','projects')
    so,er = qcurlcall(str(target),'GET',session)
           
    #Convert JSON into a python dict with literal_eval.
    #A proper return looks something like this.
    #"[{'description': '', 'URI': '/data/projects/InitialTVB', 'name': 'Initial TVB', 'secondary_ID': 'Initial TVB', 'pi_lastname': '', 'ID': 'InitialTVB', 'pi_firstname': ''}, {'description': '', 'URI': '/data/projects/TestNRG', 'name': 'NRG Data Type Tests', 'secondary_ID': 'NRG Data Tests', 'pi_lastname': '', 'ID': 'TestNRG', 'pi_firstname': ''}]"
    idAccumulator = []
    try:
        for project in literal_eval(so)['ResultSet']['Result']:
            idAccumulator.append(project['ID'])
    except SyntaxError:
        print so
        raise xnatApiException(`target`+' returned non JSON data')
    return idAccumulator
    
def getSubjects(base=None,project=None,session=None):
    if not base: base = BASE
    if not project: project = getProjects(base) 
    if type(project) != type([]): project = [project]
    
    subjectAccumulator = {}
    for p in project:
        target = urljoin(base,'data','archive','projects',p,'subjects')
        so,er = qcurlcall(str(target),'GET',session=session)

        for subject in literal_eval(so)['ResultSet']['Result']:
            sjbprj = subject['project']
            if not sjbprj in subjectAccumulator: 
                subjectAccumulator[sjbprj] = [subject['label']]
            else:
                subjectAccumulator[sjbprj].append(subject['label'])

    return subjectAccumulator
    
def getFirstProjectOfSubject(subject,projects_with_subjects):
    '''Find the first project that contains the subject. This may not
    necessarily be the correct project because the subject may have
    been shared with restricted viewing into the project it is first
    found in. This isn't something I currently care about.'''

    project = None
    for p,v in projects_with_subjects.items():
        if subject in v: 
            project = p
            break

    if not project: 
        raise xnatApiException("Could not find subject %s" % subject)
    return project

def getJSON(url,key,*keys,**kwargs):
    '''
    Return values extracted from JSON keys.
    If multiple keys are specified return a set containing tuples of the values for those keys for each "result"
    otherwise return a set containing the values for the single key for each "result"

    JSON fetched looks like:
    {"ResultSet":{"Result":
    [{"project":"Sample_DICOM","insert_user":"mmilch","ID":"CENTRAL_S00461",
    "insert_date":"2009-12-04 12:34:45.0","label":"PACE_HF_SUPINE",
    "URI":"/data/subjects/CENTRAL_S00461"},
    {"project":"Sample_DICOM","insert_user":"mmilch","ID":"CENTRAL_S01894",
    "insert_date":"2012-01-07 22:06:29.0","label":"dcmtest1",
    "URI":"/data/subjects/CENTRAL_S01894"}], 
    "totalRecords": "2"}}

    For a single key 'insert_user' return set(['mmilch','mmilch'])
    For multiple keys 'project','ID', return set(('Sample_DICOM','CENTRAL_s00461'),('Sample_DICOM','CENTRAL_s01894'))
    '''

    so,er = qcurlcall(url,method="GET",session=kwargs['session'] if 'session' in kwargs else None)

    try:resultset = literal_eval(so)
    except SyntaxError:
        print so
        raise xnatApiException(url+' returned non JSON data')

    accumulator = set()
    for i in resultset['ResultSet']['Result']:
        if len(keys) != 0:
            klist = [i[key]]
            for k in keys: klist.append(i[k])
            accumulator.add(tuple(klist))
        else: accumulator.add(i[key])
    return accumulator

def getExperiments(surl):
    '''Retrieve all experiments for the given subject. '''
    
    target = surljoin(surl,'experiments')
    so,er = qcurlcall(target,method="GET")
    
    resultset = literal_eval(so)
    experimentAcc = set()
    for exp in resultset['ResultSet']['Result']:
        experimentAcc.add((exp['label'],exp['xsiType']))
    return experimentAcc

def getScans(eurl,session):
    '''Retrieve all scans for the given experiment. '''                       
    
    target = surljoin(eurl,'scans')
    so,er = qcurlcall(target,"GET",session)
    
    resultset = literal_eval(so)
    scanAcc = set()
    for s in resultset['ResultSet']['Result']:
        scanAcc.add(s['ID'])
    return scanAcc

def getReconstructions(subject,base=None,project=None,experiment=None,URI=False):
   ''' Get the reconstructions for a subject. If the project is not
   specified then the first project where the subject is found is
   used. Note that this may return a limited view of what
   reconstructions a subject has.  See getExperiments(). If the
   experiment is not provided, return all reconstructions for all
   found experiments for the subject. Optionally return the URI instead
   of just the name if URI != False'''

   if not base: base = BASE
   if not project:
        projects_with_subjects = getSubjects(base)
        project = getFirstProjectOfSubject(subject,projects_with_subjects)

   target = urljoin(base,'data','archive',
                    'projects',project,
                    'subjects',subject)
   
   experiments = experiment if experiment else getExperiments(subject,project,base)
   expdict = {}
   #key = 'ID' if not URI else 'URI'
   for e in experiments:
       #query xml instead of using rest. see reason below.
       etarget = target.urljoin('experiments',e)
       recons = getItemURL(str(etarget.urljoin('?format=xml')), 'reconstructions')
       if URI:
           urledRecons = []
           etarget = etarget.urljoin('reconstructions')
           expdict[e] = map(lambda recon: str(etarget.urljoin(recon)), recons)
       else: expdict[e] = recons
           

       #the below code works great but xnat has a bug where experiments
       #that are not actually experiments but just forms such as the 
       #tractparameters "experiment", will not respond properly to
       #the rest interface. 
       # etarget = target.urljoin('experiments',e,'reconstructions')
       # so,er=qcurlcall(`etarget`,'GET')
       # resultset = literal_eval(so)
       # expdict[e] = []
       # for recon in resultset['ResultSet']['Result']:
       #     expdict[e].append(recon[key])
   return expdict

def combineLists(lists):
    return reduce(lambda s1,s2 : s1+s2, lists, []) 

def deleteRecon(reconstruction,subject=None,base=None):
    '''Delete the reconstruct given from the subject given.

    If the subject is not given then this function will search through
    all known subjects in order to determine who uniquely owns this 
    reconstruction. It will then delete the reconstruction.'''

    if not base: base = BASE
    #We need to determine the url path to this reconstruction. This
    #involves identifying the subject and then getting the list of
    #reconstruction URLs that this subject owns.

    #Subject not specified so we need to search through all subjects
    if not subject:
        #wow. this is ugly and non intuitive. let me explain :)
        #getSubjects() returns a dictionary that maps a project name to a list
        #of subjects in that project. Here this call takes the lists of 
        #subjects themselves and combines them into one big list
        subjects = combineLists(getSubjects(base).values())
    else: subjects = [subject]

    for s in subjects:
        #getReconstructions returns a mapping of Experiment -> [recon1,recon2..]
        recons = getReconstructions(s,base,URI=True).values()
        for r in combineLists(recons):
            #if r = /data/stuff/moar, check if moar == reconstruction
            if r.rsplit('/',1)[1] == reconstruction: 
                qcurlcall(r,method='DELETE')
                return
    raise xnatApiException("Could not find reconstruction %s"%reconstruction)
    
def deleteAllReconsSubject(surl):
    '''Delete all reconstructions for all subjects'''
    subject_urls=map(lambda subject: urljoin(surl,subject), 
                     getItemURL(surl, 'subjects'))

    exp_urls=[]
    for surl in subject_urls:
        exps = getItemURL(surl+'?format=xml','experiments')
        for exp in exps: 
            exp_urls.append(urljoin(surl,'experiments',exp))
        
    for eurl in exp_urls:
        recons = getItemURL(eurl+'?format=xml','reconstructions')
        for recon in recons:
            print recon
            target = urljoin(eurl,'reconstructions',recon)
            stdout,stderr,ret = curlcall(target,"DELETE")
            #print target,'\t...deleting'
            if ret:
                print stderr
                assert False, 'xnat error.'

#surl='https://ninja:8181/nrg/data/archive/projects/InitialTVB/subjects'
#deleteAllReconsSubject(surl)
#deleteTargetedRecon('https://ninja:8181/nrg','InitialTVB',
#                     'NRG_S00006','NRG_E00007',
#                     'fibertracking_analysis_DTI-4_T1-8_PDT2-7')
